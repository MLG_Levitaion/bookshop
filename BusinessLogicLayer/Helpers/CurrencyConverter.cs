﻿using BookShopV2.BusinessLogicLayer.Helpers.Interfaces;
using BookShopV2.BusinessLogicLayer.Models.Enums;
using System.Collections.Generic;

namespace BookShopV2.BusinessLogicLayer.Helpers
{
    public class CurrencyConverter : ICurrencyConverter
    {
        private readonly Dictionary<BLLOrderItemCurrency, decimal> _currencys = new Dictionary<BLLOrderItemCurrency, decimal>(4)
        {
            [BLLOrderItemCurrency.USD] = 1M,
            [BLLOrderItemCurrency.EUR] = 0.9M,
            [BLLOrderItemCurrency.UAH] = 23.25M,
            [BLLOrderItemCurrency.RUB] = 62.16M
        };

        public decimal ConvertPrice(decimal price, BLLOrderItemCurrency from, BLLOrderItemCurrency to)
        {
            var result = price / _currencys[from] * _currencys[to];
            return result;
        }
    }
}
