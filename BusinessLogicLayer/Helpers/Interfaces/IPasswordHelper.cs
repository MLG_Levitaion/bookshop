﻿namespace BookShopV2.BusinessLogicLayer.Helpers.Interfaces
{
    public interface IPasswordHelper
    {
        public string GeneratePassword(int length);
    }
}
