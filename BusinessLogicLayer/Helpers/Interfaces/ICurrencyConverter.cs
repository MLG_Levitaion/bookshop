﻿using BookShopV2.BusinessLogicLayer.Models.Enums;

namespace BookShopV2.BusinessLogicLayer.Helpers.Interfaces
{
    public interface ICurrencyConverter
    {
        public decimal ConvertPrice(decimal price, BLLOrderItemCurrency from, BLLOrderItemCurrency to);
    }
}
