﻿using System.Threading.Tasks;

namespace BookShopV2.BusinessLogicLayer.Helpers.Interfaces
{
    public interface IEmailHelper
    {
        public Task SendEmailAsync(string email, string subject, string message);
    }
}
