﻿using BookShopV2.BusinessLogicLayer.Extensions.ConstantsExtension.PasswordConstantsExtension;
using BookShopV2.BusinessLogicLayer.Helpers.Interfaces;
using System;
using System.Linq;

namespace BookShopV2.BusinessLogicLayer.Helpers
{
    public class PasswordHelper : IPasswordHelper
    {
        public string GeneratePassword(int length)
        {
            Random random = new Random();
            return new string(Enumerable.Repeat(PasswordConstants.PasswordCharSet, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
