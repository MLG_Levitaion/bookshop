﻿using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using BookShopV2.BusinessLogicLayer.Helpers.Interfaces;
using Microsoft.Extensions.Options;
using BookShopV2.BusinessLogicLayer.Models.Config;

namespace BookShopV2.BusinessLogicLayer.Helpers
{
    public class EmailHelper : IEmailHelper
    {
        private readonly IOptions<EmailConfiguration> _emailSettings;

        public EmailHelper(IOptions<EmailConfiguration> emailSettings)
        {
            _emailSettings = emailSettings;
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var credentials = new NetworkCredential(_emailSettings.Value.Email, _emailSettings.Value.Password); 
            var mail = new MailMessage()
            {
                From = new MailAddress(_emailSettings.Value.Email),
                Subject = subject,
                Body = message
            };
            mail.IsBodyHtml = true;
            mail.To.Add(new MailAddress(email));

            using (var client = new SmtpClient())
            {
                client.Port = _emailSettings.Value.Port;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Host = _emailSettings.Value.Host;
                client.EnableSsl = true;
                client.Credentials = credentials;
                await client.SendMailAsync(mail);
            };
        }
    }
}
