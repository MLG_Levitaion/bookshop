﻿namespace BookShopV2.BusinessLogicLayer.Extensions.AuthorMapperExtension
{
    public static partial class AuthorMapperExtension
    {
        public static Models.Authors.ResponseAuthorModelItem Map(this DataAccessLayer.Models.Authors.ResponseAuthorModelItem item)
        {
            var returnModel = new Models.Authors.ResponseAuthorModelItem();

            returnModel.Id = item.Id;
            returnModel.Name = item.Name;
            returnModel.PrintingEditions = item.PrintingEditions;

            return returnModel;
        }
    }
}
