﻿using BookShopV2.BusinessLogicLayer.Models.Authors;
using DataAccessLayer.Entities;

namespace BookShopV2.BusinessLogicLayer.Extensions.AuthorMapperExtension
{
    public static partial class AuthorMapperExtension
    {
        public static Author Map(this AuthorModelItem item)
        {
            var author = new Author();

            author.Id = item.Id;
            author.Name = item.Name;

            return author;
        }
    }
}
