﻿using System.Linq;

namespace BookShopV2.BusinessLogicLayer.Extensions.AuthorMapperExtension
{
    public static partial class AuthorMapperExtension
    {
        public static Models.Base.ResponseModel<Models.Authors.ResponseAuthorModelItem> Map(this DataAccessLayer.Models.Base.ResponseModel<DataAccessLayer.Models.Authors.ResponseAuthorModelItem> model)
        {
            var responsePrintingEditionModel = new Models.Base.ResponseModel<Models.Authors.ResponseAuthorModelItem>();
            responsePrintingEditionModel.Items = model.Items.Select(x => x.Map());
            responsePrintingEditionModel.Count = model.Count;
            return responsePrintingEditionModel;
        }
    }
}
