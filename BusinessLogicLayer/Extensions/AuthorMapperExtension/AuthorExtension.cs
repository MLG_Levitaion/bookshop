﻿using BookShopV2.BusinessLogicLayer.Models.Authors;
using DataAccessLayer.Entities;
using System.Collections.Generic;
using System.Linq;

namespace BookShopV2.BusinessLogicLayer.Extensions.AuthorMapperExtension
{
    public static partial class AuthorMapperExtension
    {
        public static ResponseAuthorModelItem Map(this Author item)
        {
            var model = new ResponseAuthorModelItem();

            model.Id = item.Id;
            model.Name = item.Name;
            model.PrintingEditions = item.AuthorInPrintingEditions.Select(x => x.PrintingEdition.Title);

            return model;
        }

        public static IEnumerable<ResponseAuthorModelItem> Map(this IEnumerable<Author> items)
        {
            var model = items.Select(a => new ResponseAuthorModelItem
            {
                Id=a.Id,
                Name = a.Name
            });
            return model;
        }

        public static void Update(this Author item, AuthorModelItem model)
        {
            item.Id = model.Id;
            item.Name = model.Name;
        }
    }
}
