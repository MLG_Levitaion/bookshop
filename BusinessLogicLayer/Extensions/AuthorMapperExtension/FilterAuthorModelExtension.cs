﻿using BookShopV2.DataAccessLayer.Models.Enums;

namespace BookShopV2.BusinessLogicLayer.Extensions.AuthorMapperExtension
{
    public static partial class AuthorMapperExtension
    {
        public static DataAccessLayer.Models.Authors.FilterAuthorModel Map(this Models.Authors.FilterAuthorModel item)
        {
            var returnModel = new DataAccessLayer.Models.Authors.FilterAuthorModel();

            returnModel.Page = item.Page;
            returnModel.PageSize = item.PageSize;
            returnModel.SearchString = item.SearchString;
            returnModel.SortField = item.SortField;
            returnModel.SortBy = (SortBy)item.SortBy;

            return returnModel;
        }
    }
}
