﻿using BookShopV2.BusinessLogicLayer.Models.Enums;
using BookShopV2.DataAccessLayer.Entities.Enums;
using System.Collections.Generic;
using System.Linq;

namespace BookShopV2.BusinessLogicLayer.Extensions.EnumMapperExtension
{
    public static partial class EnumMapperExtension
    {
        public static IEnumerable<ProductType> Map(this IEnumerable<BLLProductType> productTypes)
        {
            return productTypes.Select(x => (ProductType)x).ToList();
        }

        public static IEnumerable<OrderStatus> Map(this IEnumerable<BLLOrderStatus> orderStatuses)
        {
            return orderStatuses.Select(x => (OrderStatus)x).ToList();
        }
    }
}
