﻿namespace BookShopV2.BusinessLogicLayer.Extensions.ConstantsExtension.EmailConstantsExtension
{
    public static class EmailConstants
    {
        public const string DebugEmail = "misha.gavrilenko997@gmail.com";
        public const string EmailResetText = "Reset password";
        public const string EmailConfirmText = "Confirm your account";
    }
}
