﻿namespace BookShopV2.BusinessLogicLayer.Extensions.ConstantsExtension.PasswordConstantsExtension
{
    public static class PasswordConstants
    {
        public const string PasswordCharSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    }
}
