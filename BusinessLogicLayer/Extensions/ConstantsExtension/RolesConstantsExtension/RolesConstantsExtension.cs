﻿namespace BookShopV2.BusinessLogicLayer.Extensions.ConstantsExtension.RolesConstantsExtension
{
    public class RolesConstants
    {
        public const string UserRole = "user";
        public const string AdminRole = "admin";
    }
}
