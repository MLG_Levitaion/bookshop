﻿namespace BookShopV2.BusinessLogicLayer.Extensions.ConstantsExtension.ErrorConstantsExtension
{
    public static partial class Errors
    {
        public const string OrderItemsIsEmpty = "There is no order items";
        public const string OrderNotFound = "Order not found";
        public const string OrderNotDeleted = "Order not deleted";
        public const string OrderNotUpdated = "Order not updated";
    }
}
