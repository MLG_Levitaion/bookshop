﻿namespace BookShopV2.BusinessLogicLayer.Extensions.ConstantsExtension.ErrorConstantsExtension
{
    public static partial class Errors
    {
        public const string AuthorNameIsEmpty = "Author name is empty";
        public const string AuthorNotCreated = "Author not created";
        public const string AuthorNotFound = "Author not found";
        public const string AuthorNotDeleted = "Author not deleted";
        public const string AuthorNotUpdated = "Author not updated";
    }
}
