﻿namespace BookShopV2.BusinessLogicLayer.Extensions.ConstantsExtension.ErrorConstantsExtension
{
    public static partial class Errors
    {
        public const string UserNotCreated = "User not created";
        public const string UserNotFound = "User not found";
        public const string UserAlreadyExists = "User already exists";
        public const string UserNotUpdated = "Failed to update user";
        public const string UserNotBlocked = "Failed to block user";
        public const string UserNotUnBlocked = "Failed to unblock user";
        public const string UserNotDeleted = "Failed to delete user";
        public const string PasswordNotReseted = "Password not reseted";
        public const string InvalidPassword = "Invalid password";
        public const string TokenNotValid = "Token is not valid";
        public const string TokenIsEmpty = "Token is empty";
        public const string EmailIsEmpty = "Email is empty";
    }
}
