﻿namespace BookShopV2.BusinessLogicLayer.Extensions.ConstantsExtension.ErrorConstantsExtension
{
    public static partial class Errors
    {
        public const string PaymentIdIsEmpty = "Payment id is empty";
        public const string PaymentNotCreated = "Failed to create payment";
    }
}
