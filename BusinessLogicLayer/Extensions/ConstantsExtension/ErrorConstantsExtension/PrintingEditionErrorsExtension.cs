﻿namespace BookShopV2.BusinessLogicLayer.Extensions.ConstantsExtension.ErrorConstantsExtension
{
    public static partial class Errors
    {
        public const string PrintingEditionTitleIsEmpty = "Title is empty";
        public const string PrintingEditionNotCreated = "Failed to create printing printingEdition";
        public const string PrintingEditionNotFound = "Printing edition not found";
        public const string PrintingEditionNotUpdated = "Failed to update printing edition";
        public const string PrintingEditionNotDeleted = "Failed to delete printing edition";
    }
}
