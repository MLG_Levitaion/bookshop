﻿using BookShopV2.BusinessLogicLayer.Models.Authors;
using BookShopV2.BusinessLogicLayer.Models.Enums;
using BookShopV2.BusinessLogicLayer.Models.PrintingEditions;
using BookShopV2.DataAccessLayer.Entities.Enums;
using DataAccessLayer.Entities;
using System.Linq;

namespace BookShopV2.BusinessLogicLayer.Extensions.PrintingEditionMapperExtension
{
    public static partial class PrintingEditionMapperExtension
    {
        public static PrintingEditionModelItem Map(this PrintingEdition item)
        {
            var model = new PrintingEditionModelItem();
            model.Id = item.Id;
            model.Description = item.Description;
            model.Currency = (BLLOrderItemCurrency)item.Currency;
            model.Title = item.Title;
            model.ProductType = (BLLProductType)item.Type;
            model.Price = item.Price;
            model.Authors = item.AuthorInPrintingEditions.Select(x => new AuthorModelItem
            {
                Id = x.Author.Id,
                Name = x.Author.Name
            });

            return model;
        }

        public static void Update(this PrintingEdition item, PrintingEditionModelItem model)
        {
            item.Id = model.Id;
            item.Description = model.Description;
            item.Currency = (OrderItemCurrency)model.Currency;
            item.Title = model.Title;
            item.Type = (ProductType)model.ProductType;
            item.Price = model.Price;
        }
    }
}
