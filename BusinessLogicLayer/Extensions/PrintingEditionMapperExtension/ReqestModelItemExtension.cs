﻿using BookShopV2.BusinessLogicLayer.Models.PrintingEditions;
using BookShopV2.DataAccessLayer.Entities.Enums;
using DataAccessLayer.Entities;

namespace BookShopV2.BusinessLogicLayer.Extensions.PrintingEditionMapperExtension
{
    public static partial class PrintingEditionMapperExtension
    {
        public static PrintingEdition Map(this PrintingEditionModelItem item)
        {
            var printingEdition = new PrintingEdition();
            printingEdition.Id = item.Id;
            printingEdition.Description = item.Description;
            printingEdition.Currency = (OrderItemCurrency)item.Currency;
            printingEdition.Title = item.Title;
            printingEdition.Type = (ProductType)item.ProductType;
            printingEdition.Price = item.Price;
            return printingEdition;
        }

    }
}
