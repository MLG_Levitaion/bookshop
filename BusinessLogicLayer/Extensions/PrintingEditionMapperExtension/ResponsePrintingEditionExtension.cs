﻿using System.Linq;

namespace BookShopV2.BusinessLogicLayer.Extensions.PrintingEditionMapperExtension
{
    public static partial class PrintingEditionMapperExtension
    {
        public static Models.Base.ResponseModel<Models.PrintingEditions.PrintingEditionModelItem> Map(this DataAccessLayer.Models.Base.ResponseModel<DataAccessLayer.Models.PrintingEditions.ResponsePrintingEditionModelItem> model)
        {
            var responsePrintingEditionModel = new Models.Base.ResponseModel<Models.PrintingEditions.PrintingEditionModelItem>();
            responsePrintingEditionModel.Items = model.Items.Select(x => x.Map());
            responsePrintingEditionModel.Count = model.Count;
            return responsePrintingEditionModel;
        }
    }
}
