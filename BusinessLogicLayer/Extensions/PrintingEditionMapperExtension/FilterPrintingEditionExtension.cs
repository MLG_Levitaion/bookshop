﻿using BookShopV2.BusinessLogicLayer.Extensions.EnumMapperExtension;
using BookShopV2.DataAccessLayer.Entities.Enums;
using BookShopV2.DataAccessLayer.Models.Enums;

namespace BookShopV2.BusinessLogicLayer.Extensions.PrintingEditionMapperExtension
{
    public static partial class PrintingEditionMapperExtension
    {
        public static DataAccessLayer.Models.PrintingEditions.FilterPrintingEditionModel Map(this Models.PrintingEditions.FilterPrintingEditionModel item)
        {
            var returnModel = new DataAccessLayer.Models.PrintingEditions.FilterPrintingEditionModel();
            returnModel.Page = item.Page;
            returnModel.PageSize = item.PageSize;
            returnModel.SearchString = item.SearchString;
            returnModel.MinPrice = item.MinPrice;
            returnModel.MaxPrice = item.MaxPrice;
            returnModel.Currency = (OrderItemCurrency)item.Currency;
            returnModel.PrintingEditionTypes = item.PrintingEditionTypes.Map();
            returnModel.SortField = item.SortField;
            returnModel.SortBy = (SortBy)item.SortBy;
            return returnModel;
        }
    }
}
