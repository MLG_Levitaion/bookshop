﻿using BookShopV2.BusinessLogicLayer.Models.Enums;
using System.Linq;

namespace BookShopV2.BusinessLogicLayer.Extensions.PrintingEditionMapperExtension
{
    public static partial class PrintingEditionMapperExtension
    {
        public static Models.PrintingEditions.PrintingEditionModelItem Map(this DataAccessLayer.Models.PrintingEditions.ResponsePrintingEditionModelItem item)
        {
            var returnModel = new Models.PrintingEditions.PrintingEditionModelItem();
            returnModel.Authors = item.Authors.Select(x=> new Models.Authors.AuthorModelItem { 
                                                        Id=x.Id,
                                                        Name=x.Name
                                                        });
            returnModel.Description = item.Description;
            returnModel.Id = item.Id;
            returnModel.Price = item.Price;
            returnModel.Currency = (BLLOrderItemCurrency)item.Currency;
            returnModel.Title = item.Title;
            returnModel.ProductType = (BLLProductType)item.Type;
            return returnModel;
        }
    }
}
