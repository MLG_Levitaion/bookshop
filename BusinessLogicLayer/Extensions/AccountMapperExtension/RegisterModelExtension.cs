﻿using BookShopV2.BusinessLogicLayer.Models.Account;
using BookShopV2.DataAccessLayer.Entities;

namespace BookShopV2.BusinessLogicLayer.Extensions.AccountMapperExtension
{
    public static class RegisterModelExtension
    {
        public static ApplicationUser Map(this RegisterModel item)
        {
            var user = new ApplicationUser();

            user.ProfilePicture = item.ProfilePicture;
            user.FirstName = item.FirstName;
            user.LastName = item.LastName;
            user.UserName = item.UserName;
            user.Email = item.Email;

            return user;

        }
    }
}
