﻿using BookShopV2.BusinessLogicLayer.Extensions.EnumMapperExtension;
using BookShopV2.DataAccessLayer.Entities.Enums;
using BookShopV2.DataAccessLayer.Models.Enums;

namespace BookShopV2.BusinessLogicLayer.Extensions.OrderMapperExtension
{
    public static partial class OrderMapperExtension
    {
        public static DataAccessLayer.Models.Orders.FilterOrderModel Map(this Models.Orders.FilterOrderModel filter)
        {
            var returnModel = new DataAccessLayer.Models.Orders.FilterOrderModel();
            returnModel.Page = filter.Page;
            returnModel.PageSize = filter.PageSize;
            returnModel.SearchString = filter.SearchString;
            returnModel.SortBy = (SortBy)filter.SortBy;
            returnModel.SortField = filter.SortField;
            returnModel.IdUser = filter.IdUser;
            returnModel.Status = filter.Status.Map();
            returnModel.Currency = (OrderItemCurrency)filter.Currency;
            return returnModel;
        }
    }
}
