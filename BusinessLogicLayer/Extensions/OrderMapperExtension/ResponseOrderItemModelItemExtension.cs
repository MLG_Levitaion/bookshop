﻿using BookShopV2.BusinessLogicLayer.Models.Enums;
using System.Linq;

namespace BookShopV2.BusinessLogicLayer.Extensions.OrderMapperExtension
{
    public static partial class OrderMapperExtension
    {
        public static Models.Orders.ResponseOrderModelItem Map(this DataAccessLayer.Models.Orders.ResponseOrderModelItem model)
        {
            var returnModel = new Models.Orders.ResponseOrderModelItem();
            returnModel.UserName = model.UserName;
            returnModel.UserEmail = model.UserEmail;
            returnModel.Id = model.Id;
            returnModel.OrderAmount = model.OrderAmount;
            returnModel.OrderItems = model.OrderItems.Select(x => x.Map());
            returnModel.CreationDate = model.CreationDate.ToString("MM/dd/yyyy");
            returnModel.Status = (BLLOrderStatus)model.Status;
            return returnModel;
        }
    }
}
