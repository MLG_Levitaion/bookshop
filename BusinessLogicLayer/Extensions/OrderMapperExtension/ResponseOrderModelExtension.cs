﻿using System.Linq;

namespace BookShopV2.BusinessLogicLayer.Extensions.OrderMapperExtension
{
    public static partial class OrderMapperExtension
    {
        public static Models.Base.ResponseModel<Models.Orders.ResponseOrderModelItem> Map(this DataAccessLayer.Models.Base.ResponseModel<DataAccessLayer.Models.Orders.ResponseOrderModelItem> model)
        {
            var responsePrintingEditionModel = new Models.Base.ResponseModel<Models.Orders.ResponseOrderModelItem>();
            responsePrintingEditionModel.Items = model.Items.Select(x => x.Map());
            responsePrintingEditionModel.Count = model.Count;
            return responsePrintingEditionModel;
        }
    }
}
