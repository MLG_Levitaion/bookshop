﻿using BookShopV2.BusinessLogicLayer.Models.Enums;
using BookShopV2.BusinessLogicLayer.Models.Orders;
using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookShopV2.BusinessLogicLayer.Extensions.OrderMapperExtension
{
    public static partial class OrderMapperExtension
    {
        public static ResponseOrderModelItem Map(this Order order)
        {
            var orderModel = new ResponseOrderModelItem();
            orderModel.Id = order.Id;
            orderModel.Status = (BLLOrderStatus)order.Status;
            orderModel.UserName = order.User.UserName;
            orderModel.UserEmail = order.User.Email;
            return orderModel;
        }
    }
}
