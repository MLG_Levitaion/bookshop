﻿using BookShopV2.BusinessLogicLayer.Models.Enums;

namespace BookShopV2.BusinessLogicLayer.Extensions.OrderMapperExtension
{
    public static partial class OrderMapperExtension
    {
        public static Models.OrderItems.ResponseOrderItemModelItem Map(this DataAccessLayer.Models.OrderItems.ResponseOrderItemModelItem model)
        {
            var resultModel = new Models.OrderItems.ResponseOrderItemModelItem();

            resultModel.Count = model.Count;
            resultModel.ProductType = (BLLProductType)model.ProductType;
            resultModel.Title = model.Title;
            resultModel.Amount = model.Amount;
           
            return resultModel;
        }
    }
}
