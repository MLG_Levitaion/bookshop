﻿using BookShopV2.BusinessLogicLayer.Models.Orders;
using DataAccessLayer.Entities;

namespace BookShopV2.BusinessLogicLayer.Extensions.OrderMapperExtension
{
    public static partial class OrderMapperExtension
    {
        public static Order Map(this RequestOrderModelItem orderModelItem)
        {
            var order = new Order();
            order.Description = orderModelItem.Description;
            order.UserId = orderModelItem.UserId;
            return order;
        }
    }
}
