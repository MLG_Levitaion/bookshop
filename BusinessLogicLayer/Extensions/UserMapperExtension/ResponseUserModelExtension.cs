﻿using System.Linq;

namespace BookShopV2.BusinessLogicLayer.Extensions.UserMapperExtension
{
    public static partial class UserMapperExtension
    {
        public static Models.Base.ResponseModel<Models.Users.UserModelItem> Map(this DataAccessLayer.Models.Base.ResponseModel<DataAccessLayer.Models.Users.UserModelItem> model)
        {
            var responsePrintingEditionModel = new Models.Base.ResponseModel<Models.Users.UserModelItem>();
            responsePrintingEditionModel.Items = model.Items.Select(x => x.Map());
            responsePrintingEditionModel.Count = model.Count;
            return responsePrintingEditionModel;
        }
    }
}
