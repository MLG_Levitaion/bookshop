﻿using BookShopV2.DataAccessLayer.Models.Enums;

namespace BookShopV2.BusinessLogicLayer.Extensions.UserMapperExtension
{
    public static partial class UserMapperExtension
    {
        public static DataAccessLayer.Models.Users.FilterUserModel Map(this Models.Users.FilterUserModel item)
        {
            var returnModel = new DataAccessLayer.Models.Users.FilterUserModel();
            returnModel.Page = item.Page;
            returnModel.PageSize = item.PageSize;
            returnModel.SearchString = item.SearchString;
            returnModel.SortField = item.SortField;
            returnModel.SortBy = (SortBy)item.SortBy;
            returnModel.IsBlocked = item.IsBlocked;
            return returnModel;
        }
    }
}
