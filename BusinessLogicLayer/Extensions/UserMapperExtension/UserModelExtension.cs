﻿using BookShopV2.BusinessLogicLayer.Models.Users;
using BookShopV2.DataAccessLayer.Entities;

namespace BookShopV2.BusinessLogicLayer.Extensions.UserMapperExtension
{
    public static partial class UserMapperExtension
    {
        public static ApplicationUser Map(this UserModelItem item)
        {
            var user = new ApplicationUser();

            user.ProfilePicture = item.ProfilePicture;
            user.FirstName = item.FirstName; 
            user.LastName = item.LastName;
            user.UserName = item.UserName;
            user.Email = item.Email;
            user.IsBlocked = item.IsBlocked;

            return user;
           
        }        
    }
}
