﻿namespace BookShopV2.BusinessLogicLayer.Extensions.UserMapperExtension
{
    public static partial class UserMapperExtension
    {
        public static Models.Users.UserModelItem Map(this DataAccessLayer.Models.Users.UserModelItem item)
        {
            var returnModel = new Models.Users.UserModelItem();
            returnModel.Id = item.Id;
            returnModel.ProfilePicture = item.ProfilePicture;
            returnModel.UserName = item.UserName;
            returnModel.FirstName = item.FirstName;
            returnModel.LastName = item.LastName;
            returnModel.Email = item.Email;
            returnModel.IsBlocked = item.IsBlocked;
            return returnModel;
        }
    }
}
