﻿using BookShopV2.BusinessLogicLayer.Models.Users;
using BookShopV2.DataAccessLayer.Entities;

namespace BookShopV2.BusinessLogicLayer.Extensions.UserMapperExtension
{
    public static partial class UserMapperExtension
    {
        public static UserModelItem Map(this ApplicationUser item)
        {
            var model = new UserModelItem();

            model.Id = item.Id;
            model.ProfilePicture = item.ProfilePicture;
            model.FirstName = item.FirstName;
            model.LastName = item.LastName;
            model.UserName = item.UserName;
            model.Email = item.Email;
            model.IsBlocked = item.IsBlocked;

            return model;
        }
        public static void Update(this ApplicationUser item, UserModelItem model)
        {
            item.Id = model.Id;
            item.ProfilePicture = model.ProfilePicture;
            item.UserName = model.UserName;
            item.FirstName = model.FirstName;
            item.LastName = model.LastName;
            item.Email = model.Email;
            item.IsBlocked = model.IsBlocked;

        }
    }
}
