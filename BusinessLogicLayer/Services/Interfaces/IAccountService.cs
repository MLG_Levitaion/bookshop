﻿using BookShopV2.BusinessLogicLayer.Models.Account;
using BookShopV2.BusinessLogicLayer.Models.Base;
using BookShopV2.BusinessLogicLayer.Models.Users;
using System.Threading.Tasks;

namespace BookShopV2.BusinessLogicLayer.Services.Interfaces
{
    public interface IAccountService
    {
        public Task<UserModelItem> LoginAsync(LoginModel userModel);
        public Task<BaseModel> RegisterAsync(RegisterModel userModel);
        public Task<BaseModel> ConfirmEmailAsync(long userId, string token);
        public Task<BaseModel> EmailForgotPasswordAsync(string email);
        public Task<BaseModel> ChangePasswordAsync(ChangePasswordModel changePasswordModel);
        public Task LogOutAsync();

    }
}
