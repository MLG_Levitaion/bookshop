﻿using BookShopV2.BusinessLogicLayer.Models.Base;
using BookShopV2.BusinessLogicLayer.Models.PrintingEditions;
using System.Threading.Tasks;

namespace BookShopV2.BusinessLogicLayer.Services.Interfaces
{
    public interface IPrintingEditionService : IBaseService<PrintingEditionModelItem>
    {
        public Task<ResponseModel<PrintingEditionModelItem>> GetAllAsync(FilterPrintingEditionModel filter);
        public Task<PrintingEditionModelItem> GetAsync(long id);
        public Task<BaseModel> UpdateAsync(PrintingEditionModelItem printingEditionModel);
        public ConvertPriceModel Convert(ConvertPriceModel convertPriceModel);
    }
}
