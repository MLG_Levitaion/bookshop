﻿using BookShopV2.BusinessLogicLayer.Models.Base;
using BookShopV2.BusinessLogicLayer.Models.Orders;
using BookShopV2.BusinessLogicLayer.Models.Payments;
using System.Threading.Tasks;

namespace BookShopV2.BusinessLogicLayer.Services.Interfaces
{
    public interface IOrderService : IBaseService<RequestOrderModelItem>
    {
        public Task<ResponseModel<ResponseOrderModelItem>> GetFilteredAsync(FilterOrderModel filter);
        public Task<ResponseOrderModelItem> GetAsync(long id);
        public Task<BaseModel> UpdateAsync(PaymentModelItem payemntModel);
    }
}
