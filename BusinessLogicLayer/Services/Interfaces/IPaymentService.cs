﻿using DataAccessLayer.Entities;

namespace BookShopV2.BusinessLogicLayer.Services.Interfaces
{
    public interface IPaymentService : IBaseService<Payment>
    {
    }
}
