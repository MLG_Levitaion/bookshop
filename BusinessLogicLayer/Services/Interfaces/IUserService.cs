﻿using BookShopV2.BusinessLogicLayer.Models.Base;
using BookShopV2.BusinessLogicLayer.Models.Users;
using System.Threading.Tasks;

namespace BookShopV2.BusinessLogicLayer.Services.Interfaces
{
    public interface IUserService : IBaseService<UserModelItem>
    {
        public Task<UserModelItem> GetAsync(long id);
        public Task<ResponseModel<UserModelItem>> GetAllAsync(FilterUserModel filter);

        public Task<BaseModel> UpdateAsync(UserModelItem authorModel);

    }
}
