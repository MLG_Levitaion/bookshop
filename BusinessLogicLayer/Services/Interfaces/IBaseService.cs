﻿using BookShopV2.BusinessLogicLayer.Models.Base;
using System.Threading.Tasks;

namespace BookShopV2.BusinessLogicLayer.Services.Interfaces
{
    public interface IBaseService<T> where T : class
    {
        public Task<BaseModel> CreateAsync(T item);
        public Task<BaseModel> DeleteAsync(long id);
        
        
    }
}
