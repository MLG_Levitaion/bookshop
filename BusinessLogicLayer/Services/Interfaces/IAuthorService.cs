﻿using BookShopV2.BusinessLogicLayer.Models.Authors;
using BookShopV2.BusinessLogicLayer.Models.Base;
using System.Threading.Tasks;

namespace BookShopV2.BusinessLogicLayer.Services.Interfaces
{
    public interface IAuthorService : IBaseService<AuthorModelItem>
    {
        public Task<ResponseModel<ResponseAuthorModelItem>> GetFilteredAsync(FilterAuthorModel filter);

        public Task<ResponseModel<ResponseAuthorModelItem>> GetAllAsync();
        public Task<BaseModel> UpdateAsync(AuthorModelItem authorModel);
    }
}
