﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BookShopV2.BusinessLogicLayer.Models.Base;
using BookShopV2.BusinessLogicLayer.Services.Interfaces;
using DataAccessLayer.Entities;

namespace BookShopV2.BusinessLogicLayer.Services
{
    public class PaymentService : IPaymentService
    {
        public Task<BaseModel> CreateAsync(Payment item)
        {
            throw new NotImplementedException();
        }

        public Task<BaseModel> DeleteAsync(long id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Payment>> GetAllAsync(FilterBaseModel filter)
        {
            throw new NotImplementedException();
        }

        public Task<BaseModel> UpdateAsync(Payment item)
        {
            throw new NotImplementedException();
        }
    }
}
