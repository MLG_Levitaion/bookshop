﻿using BookShopV2.BusinessLogicLayer.Extensions.ConstantsExtension.ErrorConstantsExtension;
using BookShopV2.BusinessLogicLayer.Extensions.UserMapperExtension;
using BookShopV2.BusinessLogicLayer.Helpers.Interfaces;
using BookShopV2.BusinessLogicLayer.Models.Base;
using BookShopV2.BusinessLogicLayer.Models.Users;
using BookShopV2.BusinessLogicLayer.Services.Interfaces;
using BookShopV2.DataAccessLayer.Repositories.Interfaces;
using System.Threading.Tasks;

namespace BookShopV2.BusinessLogicLayer.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IPasswordHelper _passwordHelper;

        public UserService(IUserRepository userRepository, IPasswordHelper passwordHelper)
        {
            _userRepository = userRepository;
            _passwordHelper = passwordHelper;
        }

        public async Task<BaseModel> DeleteAsync(long userId)
        {
            var resultModel = new BaseModel();

            var result = await _userRepository.MarkAsRemovedAsync(userId);
            if (!result)
            {
                resultModel.Errors.Add(Errors.UserNotDeleted);
            }

            return resultModel;
        }

        public async Task<UserModelItem> GetAsync(long id)
        {
            var resultModel = new UserModelItem();

            var result = await _userRepository.GetById(id);
            if (result == null)
            {
                resultModel.Errors.Add(Errors.UserNotFound);
                return resultModel;
            }

            return result.Map();
        }

        public async Task<ResponseModel<UserModelItem>> GetAllAsync(FilterUserModel filter)
        {
            
            var usersList = await _userRepository.GetFilteredAsync(filter.Map());        

            return usersList.Map();
        }

        public async Task<BaseModel> UpdateAsync(UserModelItem userModel) //todo update Admin and User profile, update userProfile by Admin
        {
            var resultModel = new BaseModel();
            var user = await _userRepository.GetById(userModel.Id);
            user.Update(userModel);
            var result = await _userRepository.UpdateAsync(user);
            if (!result)
            {
                resultModel.Errors.Add(Errors.UserNotUpdated);
                return resultModel;
            }
            return resultModel;
        }
        public async Task<BaseModel> CreateAsync(UserModelItem userModel)
        {
            var resultModel = new BaseModel();
            var user = await _userRepository.GetByEmailAsync(userModel.Email);
            if (user != null)
            {
                resultModel.Errors.Add(Errors.UserAlreadyExists);
                return resultModel;
            }
            var result = await _userRepository.CreateAsync(userModel.Map(), _passwordHelper.GeneratePassword(10));
            if (result == null)
            {
                resultModel.Errors.Add(Errors.UserNotCreated);
                return resultModel;
            }
            return resultModel;
        }
                
    }
}
