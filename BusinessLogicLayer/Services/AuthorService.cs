﻿using BookShopV2.BusinessLogicLayer.Extensions.AuthorMapperExtension;
using BookShopV2.BusinessLogicLayer.Extensions.ConstantsExtension.ErrorConstantsExtension;
using BookShopV2.BusinessLogicLayer.Models.Authors;
using BookShopV2.BusinessLogicLayer.Models.Base;
using BookShopV2.BusinessLogicLayer.Services.Interfaces;
using BookShopV2.DataAccessLayer.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookShopV2.BusinessLogicLayer.Services
{
    public class AuthorService : IAuthorService
    {
        private readonly IAuthorRepository _authorRepository;

        public AuthorService(IAuthorRepository authorRepository)
        {
            _authorRepository = authorRepository;
        }

        public async Task<BaseModel> CreateAsync(AuthorModelItem authorModel)
        {
            var resultModel = new BaseModel();

            if (string.IsNullOrWhiteSpace(authorModel.Name))
            {
                resultModel.Errors.Add(Errors.AuthorNameIsEmpty);
                return resultModel;
            }

            var author = await _authorRepository.CreateAsync(authorModel.Map());

            if (authorModel == null)
            {
                resultModel.Errors.Add(Errors.AuthorNotCreated);
            }

            return resultModel;
        }

        public async Task<BaseModel> DeleteAsync(long id)
        {
            var resultModel = new BaseModel();

            var author = await _authorRepository.GetByIdAsync(id);
            if (author == null)
            {
                resultModel.Errors.Add(Errors.AuthorNotFound);
                return resultModel;
            }

            var result = await _authorRepository.MarkAsRemovedAsync(id);
            if (!result)
            {
                resultModel.Errors.Add(Errors.AuthorNotDeleted);
            }

            return resultModel;
        }

        public async Task<ResponseModel<ResponseAuthorModelItem>> GetAllAsync()
        {
            var authors = _authorRepository.GetAll();
            var responseModel = new ResponseModel<ResponseAuthorModelItem>();
            responseModel.Items = authors.Map();
            responseModel.Count = authors.Count();
            return responseModel;
        }

        public async Task<ResponseModel<ResponseAuthorModelItem>> GetFilteredAsync(FilterAuthorModel filter)
        {
            var authorModelsList = await _authorRepository.GetFilteredAsync(filter.Map());

            return authorModelsList.Map();
        }

        public async Task<BaseModel> UpdateAsync(AuthorModelItem authorModel)
        {
            var resultModel = new BaseModel();

            var author = await _authorRepository.GetByIdAsync(authorModel.Id);
            if(author == null)
            {
                resultModel.Errors.Add(Errors.AuthorNotFound);
                return resultModel;
            }

            author.Update(authorModel);

            var result = await _authorRepository.SaveChangesAsync(author);
            if (!result)
            {
                resultModel.Errors.Add(Errors.AuthorNotUpdated);
            }

            return resultModel;
        }

    }
}
