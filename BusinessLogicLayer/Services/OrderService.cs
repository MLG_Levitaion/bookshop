﻿using BookShopV2.BusinessLogicLayer.Extensions.ConstantsExtension.ErrorConstantsExtension;
using BookShopV2.BusinessLogicLayer.Extensions.OrderMapperExtension;
using BookShopV2.BusinessLogicLayer.Helpers.Interfaces;
using BookShopV2.BusinessLogicLayer.Models.Base;
using BookShopV2.BusinessLogicLayer.Models.Enums;
using BookShopV2.BusinessLogicLayer.Models.Orders;
using BookShopV2.BusinessLogicLayer.Models.Payments;
using BookShopV2.BusinessLogicLayer.Services.Interfaces;
using BookShopV2.DataAccessLayer.Entities.Enums;
using BookShopV2.DataAccessLayer.Repositories.Interfaces;
using DataAccessLayer.Entities;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BookShopV2.BusinessLogicLayer.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderItemRepository _orderItemRepository;
        private readonly IPaymentRepository _paymentRepository;
        private readonly IUserRepository _userRepository;
        private readonly IPrintingEditionRepository _printingEditionRepository;
        private readonly ICurrencyConverter _currencyConverter;

        public OrderService(
            IOrderRepository orderRepository,
            IOrderItemRepository orderItemRepository,
            IPaymentRepository paymentRepository,
            IUserRepository userRepository,
            IPrintingEditionRepository printingEditionRepository,
            ICurrencyConverter currencyConverter)
        {
            _orderRepository = orderRepository;
            _orderItemRepository = orderItemRepository;
            _paymentRepository = paymentRepository;
            _userRepository = userRepository;
            _printingEditionRepository = printingEditionRepository;
            _currencyConverter = currencyConverter;
        }

        public async Task<BaseModel> CreateAsync(RequestOrderModelItem orderModel)
        {
            var resultModel = new BaseModel();

            var user = await _userRepository.GetById(orderModel.UserId);
            if (user == null)
            {
                resultModel.Errors.Add(Errors.UserNotFound);
                return resultModel;
            }

            if (orderModel.OrderItems.Count == 0)
            {
                resultModel.Errors.Add(Errors.OrderItemsIsEmpty);
                return resultModel;
            }

            var order = await _orderRepository.CreateAsync(orderModel.Map());
            order.Status = OrderStatus.Unpaid;


            foreach (var item in orderModel.OrderItems)
            {
                var orderItem = new OrderItem();

                orderItem.PrintingEditionId = item.PrintingEditionId;
                orderItem.Count = item.Count;
                orderItem.OrderId = order.Id;
                orderItem.Amount = (await _printingEditionRepository.GetByIdAsync(item.PrintingEditionId)).Price * item.Count;

                await _orderItemRepository.CreateAsync(orderItem);
            }
            resultModel = order.Map();
            return resultModel;
        }

        public async Task<ResponseOrderModelItem> GetAsync(long id)
        {
            var responseModel = (await _orderRepository.GetByIdAsync(id)).Map();
            if (responseModel == null)
            {
                responseModel.Errors.Add(Errors.OrderNotFound);
            }
            return responseModel;
        }

        public async Task<BaseModel> DeleteAsync(long id)
        {
            var resultModel = new BaseModel();

            var result = await _orderRepository.MarkAsRemovedAsync(id);

            if (!result)
            {
                resultModel.Errors.Add(Errors.OrderNotDeleted);
            }

            return resultModel;
        }

        public async Task<ResponseModel<ResponseOrderModelItem>> GetFilteredAsync(FilterOrderModel filter)
        {
            var ordersModel = await _orderRepository.GetFilteredAsync(filter.Map());

            var result = ordersModel.Map();
            var list = result.Items.ToList();
            list.ForEach(x =>
            {
                x.OrderAmount = _currencyConverter.ConvertPrice(x.OrderAmount, BLLOrderItemCurrency.USD, filter.Currency);
                var itemlist = x.OrderItems.ToList();
                itemlist.ForEach(item =>
                {
                    item.Amount = _currencyConverter.ConvertPrice(item.Amount, BLLOrderItemCurrency.USD, filter.Currency);
                });
                x.OrderItems = itemlist;
                x.Currency = filter.Currency;
            });
            result.Items = list;
            return result;

        }

        public async Task<BaseModel> UpdateAsync(PaymentModelItem paymentModel)
        {
            var responseModel = new BaseModel();
            if (String.IsNullOrWhiteSpace(paymentModel.TransactionId))
            {
                responseModel.Errors.Add(Errors.PaymentIdIsEmpty);
                return responseModel;
            }

            var payment = new Payment();
            payment.TransactionID = paymentModel.TransactionId;
            try
            {
                payment = await _paymentRepository.CreateAsync(payment);
            }
            catch (Exception ex)
            {
                ;
            }

            if (payment == null)
            {
                responseModel.Errors.Add(Errors.PaymentNotCreated);
                return responseModel;
            }

            var order = await _orderRepository.GetByIdAsync(paymentModel.OrderId);
            if (order == null)
            {
                responseModel.Errors.Add(Errors.OrderNotFound);
                return responseModel;
            }

            order.Payment = payment;
            order.Status = OrderStatus.Paid;

            var result = await _orderRepository.SaveChangesAsync(order);
            if (!result)
            {
                responseModel.Errors.Add(Errors.OrderNotUpdated);
                return responseModel;
            }
            return responseModel;
        }

    }
}
