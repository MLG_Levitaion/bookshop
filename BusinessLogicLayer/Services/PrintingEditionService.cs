﻿using System.Threading.Tasks;
using System.Linq;
using BookShopV2.BusinessLogicLayer.Extensions.ConstantsExtension.ErrorConstantsExtension;
using BookShopV2.BusinessLogicLayer.Extensions.PrintingEditionMapperExtension;
using BookShopV2.BusinessLogicLayer.Models.Base;
using BookShopV2.BusinessLogicLayer.Models.PrintingEditions;
using BookShopV2.BusinessLogicLayer.Services.Interfaces;
using BookShopV2.DataAccessLayer.Repositories.Interfaces;
using DataAccessLayer.Entities;
using BookShopV2.BusinessLogicLayer.Helpers.Interfaces;
using BookShopV2.BusinessLogicLayer.Models.Enums;

namespace BookShopV2.BusinessLogicLayer.Services
{
    public class PrintingEditionService : IPrintingEditionService
    {
        private readonly IPrintingEditionRepository _printingEditionRepository;
        private readonly IAuthorInPrintingEditionRepository _authorInPrintingEditionRepository;
        private readonly ICurrencyConverter _currencyConverter;

        public PrintingEditionService(IPrintingEditionRepository printingEditionRepository, IAuthorInPrintingEditionRepository authorInPrintingEditionRepository, ICurrencyConverter currencyConverter)
        {
            _printingEditionRepository = printingEditionRepository;
            _authorInPrintingEditionRepository = authorInPrintingEditionRepository;
            _currencyConverter = currencyConverter;
        }

        public async Task<BaseModel> CreateAsync(PrintingEditionModelItem printingEditionModel)
        {
            var responseModel = new BaseModel();

            if (string.IsNullOrWhiteSpace(printingEditionModel.Title))
            {
                responseModel.Errors.Add(Errors.PrintingEditionTitleIsEmpty);
                return responseModel;
            }

            printingEditionModel.Price = _currencyConverter.ConvertPrice(printingEditionModel.Price, printingEditionModel.Currency, BLLOrderItemCurrency.USD);
            printingEditionModel.Currency = BLLOrderItemCurrency.USD;

            var printingEdition = await _printingEditionRepository.CreateAsync(printingEditionModel.Map());
            if (printingEdition == null)
            {
                responseModel.Errors.Add(Errors.PrintingEditionNotCreated);
                return responseModel;
            }

            foreach (var author in printingEditionModel.Authors)
            {
                var authorInPrintingEdition = new AuthorInPrintingEdition();

                authorInPrintingEdition.PrintingEditionId = printingEdition.Id;
                authorInPrintingEdition.AuthorId = author.Id;

                await _authorInPrintingEditionRepository.CreateAsync(authorInPrintingEdition);
            }

            return responseModel;
        }
        public async Task<BaseModel> DeleteAsync(long id)
        {
            var resultModel = new BaseModel();

            var result = await _printingEditionRepository.MarkAsRemovedAsync(id);
            if (!result)
            {
                resultModel.Errors.Add(Errors.PrintingEditionNotDeleted);
            }

            return resultModel;
        }

        public ConvertPriceModel Convert(ConvertPriceModel convertPriceModel)
        {
            convertPriceModel.Price = _currencyConverter.ConvertPrice(convertPriceModel.Price, convertPriceModel.Currency, BLLOrderItemCurrency.USD);
            convertPriceModel.Currency = BLLOrderItemCurrency.USD;
            return convertPriceModel;
        }

        public async Task<PrintingEditionModelItem> GetAsync(long id)
        {
            var result = await _printingEditionRepository.GetByIdAsync(id);
            if (result == null)
            {
                var resultModel = new PrintingEditionModelItem();
                resultModel.Errors.Add(Errors.PrintingEditionNotFound);
            }

            return result.Map();
        }

        public async Task<BaseModel> UpdateAsync(PrintingEditionModelItem printingEditionModel)
        {
            var resultModel = new BaseModel();

            var printingEdition = await _printingEditionRepository.GetByIdAsync(printingEditionModel.Id);
            if (printingEdition == null)
            {
                resultModel.Errors.Add(Errors.PrintingEditionNotFound);
                return resultModel;
            }

            printingEdition.Update(printingEditionModel);

            foreach (var author in printingEdition.AuthorInPrintingEditions.ToList())
            {
                var authorInEdition = await _authorInPrintingEditionRepository.GetByIdAsync(author.AuthorId, printingEdition.Id);
                await _authorInPrintingEditionRepository.RemoveAsync(authorInEdition.AuthorId, authorInEdition.PrintingEditionId);
            }

            foreach (var author in printingEditionModel.Authors.ToList())
            {
                var authorInPrintingEdition = new AuthorInPrintingEdition();
                authorInPrintingEdition.AuthorId = author.Id;
                authorInPrintingEdition.PrintingEditionId = printingEdition.Id;
                await _authorInPrintingEditionRepository.CreateAsync(authorInPrintingEdition);
            }

            var result = await _printingEditionRepository.SaveChangesAsync(printingEdition);
            if (!result)
            {
                resultModel.Errors.Add(Errors.PrintingEditionNotUpdated);
                return resultModel;
            }

            return resultModel;
        }

        public async Task<ResponseModel<PrintingEditionModelItem>> GetAllAsync(FilterPrintingEditionModel filter)
        {
            var printingEditionsModel = await _printingEditionRepository.GetFilteredAsync(filter.Map());
            var result = printingEditionsModel.Map();
            var list = result.Items.ToList();
            list.ForEach(x =>
             {
                 x.Price = _currencyConverter.ConvertPrice(x.Price, x.Currency, filter.Currency);
                 x.Currency = filter.Currency;
             });
            result.Items = list;
            return result;
        }
    }
}
