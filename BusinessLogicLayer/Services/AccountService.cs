﻿using BookShopV2.BusinessLogicLayer.Models.Users;
using BookShopV2.BusinessLogicLayer.Services.Interfaces;
using System.Threading.Tasks;
using BookShopV2.DataAccessLayer.Repositories.Interfaces;
using BookShopV2.BusinessLogicLayer.Helpers.Interfaces;
using BookShopV2.BusinessLogicLayer.Extensions.UserMapperExtension;
using BookShopV2.BusinessLogicLayer.Models.Base;
using Microsoft.Extensions.Configuration;
using BookShopV2.BusinessLogicLayer.Extensions.ConstantsExtension.ErrorConstantsExtension;
using BookShopV2.BusinessLogicLayer.Extensions.ConstantsExtension.EmailConstantsExtension;
using BookShopV2.BusinessLogicLayer.Extensions.AccountMapperExtension;
using BookShopV2.BusinessLogicLayer.Models.Account;
using System;

namespace BookShopV2.BusinessLogicLayer.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUserRepository _userRepository;
        private readonly IEmailHelper _emailHelper;
        private readonly IConfiguration _configuration;
        private readonly IPasswordHelper _passwordHelper;
        public AccountService(IUserRepository userRepository, IEmailHelper emailHelper, IPasswordHelper passwordHelper, IConfiguration configuration)
        {
            _userRepository = userRepository;
            _emailHelper = emailHelper;
            _passwordHelper = passwordHelper;
            _configuration = configuration;
        }

        public async Task<BaseModel> ConfirmEmailAsync(long userId, string token)
        {
            var resultModel = new BaseModel();

            if (string.IsNullOrWhiteSpace(token))
            {
                resultModel.Errors.Add(Errors.TokenIsEmpty);
                return resultModel;
            }

            token = token.Replace(' ', '+');

            var user = await _userRepository.GetById(userId);
            if (user == null)
            {
                resultModel.Errors.Add(Errors.UserNotFound);
                return resultModel;
            }

            var result = await _userRepository.ConfirmEmail(user, token);
            if (!result)
            {
                resultModel.Errors.Add(Errors.TokenNotValid);
            }

            return resultModel;
        }

        public async Task<BaseModel> EmailForgotPasswordAsync(string email)
        {
            var resultModel = new BaseModel();

            if (string.IsNullOrWhiteSpace(email))
            {
                resultModel.Errors.Add(Errors.EmailIsEmpty);
                return resultModel;
            }

            var user = await _userRepository.GetByEmailAsync(email);
            if (user == null)
            {
                resultModel.Errors.Add(Errors.UserNotFound);
                return resultModel;
            }

            string newPassword = _passwordHelper.GeneratePassword(10);
            var result = await _userRepository.ResetPasswordAsync(user, newPassword);
            if (!result)
            {
                resultModel.Errors.Add(Errors.PasswordNotReseted);
                return resultModel;
            }

            await _emailHelper.SendEmailAsync(EmailConstants.DebugEmail, EmailConstants.EmailResetText, $@"<h2>{newPassword}</h2>");

            return resultModel;
        }

        public async Task<BaseModel> ChangePasswordAsync(ChangePasswordModel changePasswordModel)
        {
            var resultModel = new UserModelItem();

            var user = await _userRepository.GetById(changePasswordModel.Id);
            if (user == null)
            {
                resultModel.Errors.Add(Errors.UserNotFound);
                return resultModel;
            }

            var result = await _userRepository.ChangePasswordAsync(user, changePasswordModel.OldPassword, changePasswordModel.NewPassword);
            if (!result)
            {
                resultModel.Errors.Add(Errors.PasswordNotReseted);
                return resultModel;
            }
            return user.Map();

        }

        public async Task<UserModelItem> LoginAsync(LoginModel userModel)
        {
            var resultModel = new UserModelItem();

            var user = await _userRepository.GetByEmailAsync(userModel.Email);
            if (user == null)
            {
                resultModel.Errors.Add(Errors.UserNotFound);
                return resultModel;
            }

            var result = await _userRepository.PasswordSignInAsync(user, userModel.Password);
            if (!result)
            {
                resultModel.Errors.Add(Errors.InvalidPassword);
                return resultModel;
            }

            resultModel = user.Map();
            resultModel.Role = await _userRepository.GetUserRole(user);

            return resultModel;

        }

        public async Task<BaseModel> RegisterAsync(RegisterModel registerModel)
        {
            var resultModel = new BaseModel();

            var user = await _userRepository.GetByEmailAsync(registerModel.Email);
            if (user != null)
            {
                resultModel.Errors.Add(Errors.UserAlreadyExists);
                return resultModel;
            }

            user = await _userRepository.CreateAsync(registerModel.Map(), registerModel.Password);
            if (user == null)
            {
                resultModel.Errors.Add(Errors.UserNotCreated);
                return resultModel;
            }

            string token = await _userRepository.GenerateEmailConfirmTokenAsync(user);
            await _emailHelper.SendEmailAsync(EmailConstants.DebugEmail, EmailConstants.EmailConfirmText, $@"<h2>" + _configuration["LocalHost"] + $@"/api/account/emailconfirm?id={user.Id}&&token={token}</h2>");

            return resultModel;
        }

        public async Task LogOutAsync()
        {
            await _userRepository.LogOutAsync();
        }


    }
}
