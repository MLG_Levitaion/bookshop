﻿using BookShopV2.BusinessLogicLayer.Models.Enums;

namespace BookShopV2.BusinessLogicLayer.Models.OrderItems
{
    public class ResponseOrderItemModelItem
    {
        public string Title { get; set; }
        public BLLProductType ProductType { get; set; }
        public int Count { get; set; }
        public decimal Amount { get; set; }
    }
}
