﻿using BookShopV2.BusinessLogicLayer.Models.Base;
using BookShopV2.BusinessLogicLayer.Models.Enums;

namespace BookShopV2.BusinessLogicLayer.Models.OrderItems
{
    public class RequestOrderItemModelItem : BaseModel
    {        
       
        public long PrintingEditionId { get; set; }
        public int Count { get; set; }
    }
}
