﻿using BookShopV2.BusinessLogicLayer.Models.Authors;
using BookShopV2.BusinessLogicLayer.Models.Base;
using BookShopV2.BusinessLogicLayer.Models.Enums;
using System.Collections.Generic;

namespace BookShopV2.BusinessLogicLayer.Models.PrintingEditions
{
    public class PrintingEditionModelItem : BaseModel
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public BLLOrderItemCurrency Currency { get; set; }
        public BLLProductType ProductType { get; set; }
        public IEnumerable<AuthorModelItem>  Authors { get; set; } = new List<AuthorModelItem>();
        
    }
}
