﻿using BookShopV2.BusinessLogicLayer.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookShopV2.BusinessLogicLayer.Models.PrintingEditions
{
    public class ConvertPriceModel
    {
        public decimal Price { get; set; }
        public BLLOrderItemCurrency Currency { get; set; }
    }
}
