﻿using BookShopV2.BusinessLogicLayer.Models.Base;
using BookShopV2.BusinessLogicLayer.Models.Enums;
using System.Collections.Generic;

namespace BookShopV2.BusinessLogicLayer.Models.PrintingEditions
{
    public class FilterPrintingEditionModel : FilterBaseModel
    {
        public decimal MinPrice { get; set; } = 0;
        public decimal MaxPrice { get; set; } = int.MaxValue;
        public IEnumerable<BLLProductType> PrintingEditionTypes { get; set; } = new List<BLLProductType>();
        public BLLOrderItemCurrency Currency { get; set; }
    }
}
