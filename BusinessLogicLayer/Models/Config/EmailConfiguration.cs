﻿namespace BookShopV2.BusinessLogicLayer.Models.Config
{
    public class EmailConfiguration
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public string Host { get; set; }
    }
}
