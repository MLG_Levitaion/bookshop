﻿using System.Collections.Generic;

namespace BookShopV2.BusinessLogicLayer.Models.Authors
{
    public class ResponseAuthorModelItem : AuthorModelItem
    {
        public IEnumerable<string> PrintingEditions { get; set; } = new List<string>();
    }
}
