﻿using BookShopV2.BusinessLogicLayer.Models.Base;

namespace BookShopV2.BusinessLogicLayer.Models.Authors
{
    public class AuthorModelItem : BaseModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        
    }
}
