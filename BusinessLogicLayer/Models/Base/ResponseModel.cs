﻿using System.Collections.Generic;

namespace BookShopV2.BusinessLogicLayer.Models.Base
{
    public class ResponseModel<T> where T : class
    {
        public int Count { get; set; }
        public IEnumerable<T> Items { get; set; } = new List<T>();
    }
}
