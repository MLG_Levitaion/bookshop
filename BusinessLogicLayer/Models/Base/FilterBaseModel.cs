﻿using BookShopV2.BusinessLogicLayer.Models.Enums;

namespace BookShopV2.BusinessLogicLayer.Models.Base
{
    public class FilterBaseModel
    {
        public string SearchString { get; set; } = string.Empty;
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string SortField { get; set; } = "id";
        public BLLSortBy SortBy { get; set; }
    }
}
