﻿using System.Collections.Generic;

namespace BookShopV2.BusinessLogicLayer.Models.Base
{
    public class BaseModel
    {
        public ICollection<string> Errors { get; set; } = new List<string>();
    }
}
