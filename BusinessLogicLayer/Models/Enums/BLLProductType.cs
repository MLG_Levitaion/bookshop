﻿namespace BookShopV2.BusinessLogicLayer.Models.Enums
{
    public enum BLLProductType
    {
        Book =1,
        Magazine = 2,
        NewsPaper = 3
    }
}
