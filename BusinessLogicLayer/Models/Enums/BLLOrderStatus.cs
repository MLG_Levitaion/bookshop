﻿namespace BookShopV2.BusinessLogicLayer.Models.Enums
{
    public enum BLLOrderStatus
    {
        None = 0,
        Unpaid = 1,
        Paid = 2
    }
}
