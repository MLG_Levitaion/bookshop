﻿namespace BookShopV2.BusinessLogicLayer.Models.Enums
{
    public enum BLLOrderItemCurrency
    {
        USD = 0,
        UAH = 1,
        EUR = 2,
        RUB = 3
    }
}
