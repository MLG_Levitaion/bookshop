﻿namespace BookShopV2.BusinessLogicLayer.Models.Enums
{
    public enum BLLSortBy
    {
        None = 0,
        Ascending = 1,
        Descending = 2
    }
}
