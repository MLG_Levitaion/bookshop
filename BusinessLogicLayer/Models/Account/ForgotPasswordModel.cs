﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookShopV2.BusinessLogicLayer.Models.Account
{
    public class ForgotPasswordModel
    {
        public string Email { get; set; }
    }
}
