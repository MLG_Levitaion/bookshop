﻿namespace BookShopV2.BusinessLogicLayer.Models.Account
{
    public class ChangePasswordModel
    {
        public long Id { get; set; }
        public string NewPassword { get; set; }
        public string OldPassword { get; set; }
    }
}
