﻿using BookShopV2.BusinessLogicLayer.Models.Base;
using BookShopV2.BusinessLogicLayer.Models.Enums;
using BookShopV2.BusinessLogicLayer.Models.OrderItems;
using System;
using System.Collections.Generic;

namespace BookShopV2.BusinessLogicLayer.Models.Orders
{
    public class ResponseOrderModelItem : BaseModel
    {
        public long Id { get; set; }
        public string CreationDate { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public decimal OrderAmount { get; set; }
        public BLLOrderStatus Status { get; set; }
        public BLLOrderItemCurrency Currency { get; set; }
        public IEnumerable<ResponseOrderItemModelItem> OrderItems { get; set; } = new List<ResponseOrderItemModelItem>();


    }
}
