﻿using BookShopV2.BusinessLogicLayer.Models.Base;
using BookShopV2.BusinessLogicLayer.Models.Enums;
using System.Collections.Generic;

namespace BookShopV2.BusinessLogicLayer.Models.Orders
{
    public class FilterOrderModel : FilterBaseModel
    {
        public BLLOrderItemCurrency Currency { get; set; }
        public IEnumerable<BLLOrderStatus> Status { get; set; }
        public int IdUser { get; set; }
    }
}
