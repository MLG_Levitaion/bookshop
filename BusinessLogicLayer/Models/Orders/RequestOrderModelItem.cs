﻿using BookShopV2.BusinessLogicLayer.Models.Base;
using BookShopV2.BusinessLogicLayer.Models.Enums;
using BookShopV2.BusinessLogicLayer.Models.OrderItems;
using System.Collections.Generic;

namespace BookShopV2.BusinessLogicLayer.Models.Orders
{
    public class RequestOrderModelItem : BaseModel
    {
        public BLLOrderItemCurrency Currency { get; set; }
        public string Description { get; set; }
        public long UserId { get; set; } 
        public ICollection<RequestOrderItemModelItem> OrderItems { get; set; } = new List<RequestOrderItemModelItem>();
    }
}
