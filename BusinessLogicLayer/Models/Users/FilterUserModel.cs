﻿using BookShopV2.BusinessLogicLayer.Models.Base;

namespace BookShopV2.BusinessLogicLayer.Models.Users
{
    public class FilterUserModel : FilterBaseModel
    {
        public bool IsBlocked { get; set; }      
    }
}
