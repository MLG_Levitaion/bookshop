﻿using BookShopV2.BusinessLogicLayer.Models.Base;
 
namespace BookShopV2.BusinessLogicLayer.Models.Payments
{
    public class PaymentModelItem : BaseModel
    {
        public long OrderId { get; set; }
        public string TransactionId { get; set; }
    }
}
