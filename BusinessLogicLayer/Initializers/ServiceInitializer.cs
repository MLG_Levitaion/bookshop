﻿using BookShopV2.BusinessLogicLayer.Helpers.Interfaces;
using BookShopV2.BusinessLogicLayer.Helpers;
using BookShopV2.BusinessLogicLayer.Services.Interfaces;
using BookShopV2.BusinessLogicLayer.Services;
using BookShopV2.BusinessLogicLayer.Models.Config;
using Microsoft.Extensions.DependencyInjection;
using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.OpenApi.Models;
using BookShopV2.DataAccessLayer.Initialize;

namespace BookShopV2.BusinessLogicLayer.Initializers
{

    public static class ServiceInitializer
    {
        public static void Initialize(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddCors();            
            services.AddEntityFrameworkSqlServer();
            services.AddControllers();

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();


            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = configuration["JwtConfiguration:JwtIssuer"],
                        ValidAudience = configuration["JwtConfiguration:JwtIssuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JwtConfiguration:JwtKey"])),
                        ClockSkew = TimeSpan.Zero
                    };
                });

            services.AddAuthorization();

            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IPaymentService, PaymentService>();
            services.AddScoped<IAuthorService, AuthorService>();
            services.AddScoped<IPrintingEditionService, PrintingEditionService>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IUserService, UserService>();

            services.AddScoped<IPasswordHelper,PasswordHelper>();
            services.AddScoped<IEmailHelper, EmailHelper>();
            services.AddScoped<ICurrencyConverter, CurrencyConverter>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(configuration["SwaggerConfiguration:Version"], new OpenApiInfo { Title = configuration["SwaggerConfiguration:Name"], Version = configuration["SwaggerConfiguration:Version"] });
            });

            services.Configure<EmailConfiguration>(configuration.GetSection("Emailconfiguration"));

            services.AddMvcCore().AddApiExplorer();

            DbServiceInitializer.Initialize(services,configuration);

        }
    }
}
