﻿using BookShopV2.DataAccessLayer.Models.Authors;
using BookShopV2.DataAccessLayer.Repositories.Base;
using BookShopV2.DataAccessLayer.Repositories.Interfaces;
using DataAccessLayer.AppContext;
using DataAccessLayer.Entities;
using System.Linq;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using BookShopV2.DataAccessLayer.Models.Base;
using BookShopV2.DataAccessLayer.Models.Enums;
using System;

namespace BookShopV2.DataAccessLayer.Repositories.EFRepositories
{
    public class AuthorRepository : BaseRepository<Author>, IAuthorRepository
    {
        public AuthorRepository(ApplicationDbContext db) : base(db)
        {

        }

        public async Task<ResponseModel<ResponseAuthorModelItem>> GetFilteredAsync(FilterAuthorModel filter)
        {
            var responseModel = new ResponseModel<ResponseAuthorModelItem>();
            var list = _dbContextSet.Where(author =>
                                    (author.Name.Contains(filter.SearchString) ||
                                     author.AuthorInPrintingEditions.Any(e => e.PrintingEdition.Title.Contains(filter.SearchString)))
                                     &&
                                     author.IsRemoved == false)
                                    .Select(author => new ResponseAuthorModelItem
                                    {
                                        Id = author.Id,
                                        Name = author.Name,
                                        PrintingEditions = author.AuthorInPrintingEditions.Select(edition => edition.PrintingEdition.Title)
                                    });

            var sortField = Type.GetType(typeof(Author).ToString(), false, true).GetProperties().FirstOrDefault(type => type.Name.ToUpper() == filter.SortField.ToUpper());

            if (filter.SortBy != SortBy.None && sortField != null)
            {
                list = list.OrderBy($"{sortField.Name} {filter.SortBy}").AsQueryable();
            }

            responseModel.Count = await list.CountAsync();

            responseModel.Items = await list.Skip(filter.PageSize * (filter.Page - 1)).Take(filter.PageSize).ToListAsync();

            return responseModel;
        }

    }
}
