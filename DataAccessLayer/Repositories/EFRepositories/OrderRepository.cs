﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using BookShopV2.DataAccessLayer.Entities.Enums;
using BookShopV2.DataAccessLayer.Extensions;
using BookShopV2.DataAccessLayer.Extensions.PredicateBuilder;
using BookShopV2.DataAccessLayer.Models.Base;
using BookShopV2.DataAccessLayer.Models.Enums;
using BookShopV2.DataAccessLayer.Models.OrderItems;
using BookShopV2.DataAccessLayer.Models.Orders;
using BookShopV2.DataAccessLayer.Repositories.Base;
using BookShopV2.DataAccessLayer.Repositories.Interfaces;
using DataAccessLayer.AppContext;
using DataAccessLayer.Entities;
using Microsoft.EntityFrameworkCore;

namespace BookShopV2.DataAccessLayer.Repositories.EFRepositories
{
    public class OrderRepository : BaseRepository<Order>, IOrderRepository
    {

        public OrderRepository(ApplicationDbContext db) : base(db)
        {

        }

        public async Task<ResponseModel<ResponseOrderModelItem>> GetFilteredAsync(FilterOrderModel filter)
        {

            var resultModel = new ResponseModel<ResponseOrderModelItem>();
            var orders = _dbContextSet.Where(order =>
                                            (order.User.UserName.Contains(filter.SearchString) ||
                                            order.User.Email.Contains(filter.SearchString)) &&
                                            (filter.IdUser == 0 || order.User.Id == filter.IdUser)
                                            )
                                            .Select(order => new ResponseOrderModelItem
                                            {
                                                Id = order.Id,
                                                OrderAmount = order.OrderItems.Select(authorInEdition => authorInEdition.PrintingEdition.Price * authorInEdition.Count).Sum(),
                                                UserEmail = order.User.Email,
                                                UserName = order.User.UserName,
                                                CreationDate = order.CreationDate,
                                                OrderItems = order.OrderItems.Select(orderItem => new ResponseOrderItemModelItem
                                                {
                                                    Count = orderItem.Count,
                                                    ProductType = orderItem.PrintingEdition.Type,
                                                    Title = orderItem.PrintingEdition.Title,
                                                    Amount = orderItem.Amount
                                                }),
                                                Status = order.Status
                                            });

            string predicate = filter.Status.BuildPredicate(Constants.Status);
            if (!string.IsNullOrWhiteSpace(predicate))
            {
                orders = orders.Where(predicate);
            }
            if (filter.SortBy != SortBy.None)
            {
                orders = orders.OrderBy($"{filter.SortField} {filter.SortBy}").AsQueryable();
            }
            resultModel.Count = await orders.CountAsync();
            resultModel.Items = await orders.Skip((filter.Page - 1) * filter.PageSize).Take(filter.PageSize).ToListAsync();

            return resultModel;

        }

    }
}
