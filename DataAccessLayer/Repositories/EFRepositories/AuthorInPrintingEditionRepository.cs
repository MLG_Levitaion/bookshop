﻿using BookShopV2.DataAccessLayer.Repositories.Base;
using BookShopV2.DataAccessLayer.Repositories.Interfaces;
using DataAccessLayer.AppContext;
using DataAccessLayer.Entities;
using System.Threading.Tasks;

namespace BookShopV2.DataAccessLayer.Repositories.EFRepositories
{
    public class AuthorInPrintingEditionRepository : BaseRepository<AuthorInPrintingEdition>, IAuthorInPrintingEditionRepository
    {
        public AuthorInPrintingEditionRepository(ApplicationDbContext db) : base(db)
        {

        }
        public async Task<AuthorInPrintingEdition> GetByIdAsync(long authorId, long printingEditionId)
        {
            var result = await _dbContextSet.FindAsync(authorId,printingEditionId);
            return result;
        }
        public async Task<bool> RemoveAsync(long authorId, long printingEditionId)
        {
            var item = await _dbContextSet.FindAsync(authorId,printingEditionId);
            if (item == null)
            {
                return false;
            }
            _dbContextSet.Remove(item);
            return await _dbContext.SaveChangesAsync() > 0;
        }
    }
}
