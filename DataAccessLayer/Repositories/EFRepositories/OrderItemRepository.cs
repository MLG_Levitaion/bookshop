﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BookShopV2.DataAccessLayer.Models.Orders;
using BookShopV2.DataAccessLayer.Repositories.Base;
using BookShopV2.DataAccessLayer.Repositories.Interfaces;
using DataAccessLayer.AppContext;
using DataAccessLayer.Entities;

namespace BookShopV2.DataAccessLayer.Repositories.EFRepositories
{
    public class OrderItemRepository : BaseRepository<OrderItem>,IOrderItemRepository
    {
        public OrderItemRepository(ApplicationDbContext db) : base(db)
        {
        }
        public async Task<IEnumerable<OrderItem>> GetAllAsync(FilterOrderModel filter)
        {
            return _dbContextSet;
        }

    }
}
