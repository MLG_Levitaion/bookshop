﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using BookShopV2.DataAccessLayer.Models.PrintingEditions;
using BookShopV2.DataAccessLayer.Repositories.Base;
using BookShopV2.DataAccessLayer.Repositories.Interfaces;
using DataAccessLayer.AppContext;
using DataAccessLayer.Entities;
using BookShopV2.DataAccessLayer.Models.Base;
using BookShopV2.DataAccessLayer.Models.Enums;
using System;
using BookShopV2.DataAccessLayer.Extensions.PredicateBuilder;
using BookShopV2.DataAccessLayer.Extensions;
using BookShopV2.DataAccessLayer.Models.Authors;

namespace BookShopV2.DataAccessLayer.Repositories.EFRepositories
{
    public class PrintingEditionRepository : BaseRepository<PrintingEdition>, IPrintingEditionRepository
    {
        public PrintingEditionRepository(ApplicationDbContext db) : base(db)
        {

        }

        public async Task<ResponseModel<ResponsePrintingEditionModelItem>> GetFilteredAsync(FilterPrintingEditionModel filter)
        {
            var resultModel = new ResponseModel<ResponsePrintingEditionModelItem>();
            if (filter.MaxPrice == 0)
            {
                filter.MaxPrice = int.MaxValue;
            }
            var printingEditions = _dbContextSet
                                    .Where(edition =>
                                        (edition.Title.Contains(filter.SearchString) ||
                                         edition.AuthorInPrintingEditions.Any(x => x.Author.Name.Contains(filter.SearchString) && x.Author.IsRemoved == false))
                                        && edition.Price >= filter.MinPrice && edition.Price <= filter.MaxPrice
                                        && edition.IsRemoved == false)
                                    .Select(edition => new ResponsePrintingEditionModelItem
                                    {
                                        Id = edition.Id,
                                        Title = edition.Title,
                                        Description = edition.Description,
                                        Currency = edition.Currency,
                                        Type = edition.Type,
                                        Price = edition.Price,
                                        Authors = edition.AuthorInPrintingEditions.Select(authorInEdition => new AuthorModelItem { 
                                            Id=authorInEdition.Author.Id,
                                            Name=authorInEdition.Author.Name
                                        })
                                    });

            string predicate = filter.PrintingEditionTypes.BuildPredicate(Constants.Type);
            if (!string.IsNullOrWhiteSpace(predicate))
            {
                printingEditions = printingEditions.Where(predicate);
            }

            var sortField = Type.GetType(typeof(PrintingEdition).ToString(), false, true).GetProperties().FirstOrDefault(type => type.Name.ToUpper() == filter.SortField.ToUpper());

            if (filter.SortBy != SortBy.None)
            {
                printingEditions = printingEditions.OrderBy($"{sortField.Name} {filter.SortBy}").AsQueryable();
            }

            resultModel.Count = await printingEditions.CountAsync();

            printingEditions = printingEditions.Skip(filter.PageSize * (filter.Page - 1)).Take(filter.PageSize);
            resultModel.Items = await printingEditions.ToListAsync();

            return resultModel;
        }
        
    }
}