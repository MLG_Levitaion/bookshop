﻿using System;
using System.Threading.Tasks;
using BookShopV2.DataAccessLayer.Entities;
using BookShopV2.DataAccessLayer.Repositories.Interfaces;
using DataAccessLayer.AppContext;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using System.Linq.Dynamic.Core;
using BookShopV2.DataAccessLayer.Models.Users;
using BookShopV2.DataAccessLayer.Extensions;
using BookShopV2.DataAccessLayer.Models.Base;
using Microsoft.EntityFrameworkCore;
using BookShopV2.DataAccessLayer.Models.Enums;

namespace BookShopV2.DataAccessLayer.Repositories.EFRepositories
{
    public class UserRepository : IUserRepository
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _dbContext;
        public UserRepository(ApplicationDbContext dbContext,
            SignInManager<ApplicationUser> signInManager,
            UserManager<ApplicationUser> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _dbContext = dbContext;
        }   

        public async Task<string> GenerateEmailConfirmTokenAsync(ApplicationUser user)
        {
            return await _userManager.GenerateEmailConfirmationTokenAsync(user);
        }

        public async Task<ApplicationUser> CreateAsync(ApplicationUser user, string password)
        {
            user.CreationDate = DateTime.Now;
            user.IsBlocked = false;
            var resultCreate = await _userManager.CreateAsync(user, password);

            if (!resultCreate.Succeeded)
            {
                return null;
            }

            var resultAddToRole = await _userManager.AddToRoleAsync(user, Constants.UserRole);
            if (!resultAddToRole.Succeeded)
            {
                return null;
            }
            return user;
        }

        public async Task<ApplicationUser> GetByEmailAsync(string email)
        {
            return await _userManager.FindByEmailAsync(email);
        }

        public async Task<bool> PasswordSignInAsync(ApplicationUser user, string password)
        {
            var result = await _signInManager.PasswordSignInAsync(user, password, isPersistent: false, lockoutOnFailure: false);
            return result.Succeeded;
        }

        public async Task LogOutAsync()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<bool> ConfirmEmail(ApplicationUser user, string token)
        {
            return (await _userManager.ConfirmEmailAsync(user, token)).Succeeded;
        }

        public async Task<bool> ChangePasswordAsync(ApplicationUser user, string oldPassword, string newPassword)
        {
            user = await _userManager.FindByIdAsync(user.Id.ToString());
            var result = await _userManager.ChangePasswordAsync(user, oldPassword, newPassword);

            return result.Succeeded;
        }
        public async Task<bool> ResetPasswordAsync(ApplicationUser user, string newPassword)
        {
            string token = await _userManager.GeneratePasswordResetTokenAsync(user);
            var result = await _userManager.ResetPasswordAsync(user, token, newPassword);

            return result.Succeeded;
        }
        public async Task<bool> MarkAsRemovedAsync(long id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            if (user == null)
            {
                return false;
            }
            user.IsRemoved = true;
            var result = await _userManager.UpdateAsync(user);
            return result.Succeeded;
        }

        public async Task<string> GetUserRole(ApplicationUser user)
        {
            var role = await _userManager.GetRolesAsync(user);
            return role.FirstOrDefault();
        }

        public async Task<ResponseModel<UserModelItem>> GetFilteredAsync(FilterUserModel filter)
        {
            var responseModel = new ResponseModel<UserModelItem>();

            var users = _dbContext.Users.Where(user =>
                                        user.Email != Constants.AdminEmail &&
                                        user.IsBlocked == filter.IsBlocked &&
                                        user.IsRemoved == false &&
                                        (
                                        user.UserName.Contains(filter.SearchString) ||
                                        user.FirstName.Contains(filter.SearchString) ||
                                        user.LastName.Contains(filter.SearchString) ||
                                        user.Email.Contains(filter.SearchString)
                                        ))
                                        .Select(user => new UserModelItem
                                        {
                                            Id = user.Id,
                                            ProfilePicture = user.ProfilePicture,
                                            UserName = user.UserName,
                                            FirstName = user.FirstName,
                                            LastName = user.LastName,
                                            Email = user.Email,
                                            IsBlocked = user.IsBlocked
                                        }) ;

            var sortField = Type.GetType(typeof(ApplicationUser).ToString(), false, true).GetProperties().FirstOrDefault(type => type.Name.ToUpper() == filter.SortField.ToUpper());

            if (filter.SortBy != SortBy.None)
            {
                users = users.OrderBy($"{sortField.Name} {filter.SortBy}").AsQueryable();
            }

            responseModel.Count = await users.CountAsync();

            users = users.Skip(filter.PageSize * (filter.Page - 1)).Take(filter.PageSize);
            responseModel.Items = await users.ToListAsync();

            return responseModel;
        }

        public async Task<ApplicationUser> GetById(long userId)
        {
            return await _userManager.FindByIdAsync(userId.ToString());
        }

        public async Task<bool> UpdateAsync(ApplicationUser user)
        {
            var result = await _userManager.UpdateAsync(user);
            return result.Succeeded;

        }
    }
}
