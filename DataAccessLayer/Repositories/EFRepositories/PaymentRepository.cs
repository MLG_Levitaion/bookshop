﻿using BookShopV2.DataAccessLayer.Repositories.Base;
using BookShopV2.DataAccessLayer.Repositories.Interfaces;
using DataAccessLayer.AppContext;
using DataAccessLayer.Entities;

namespace BookShopV2.DataAccessLayer.Repositories.EFRepositories
{
    public class PaymentRepository : BaseRepository<Payment>, IPaymentRepository
    {
        public PaymentRepository(ApplicationDbContext db) : base(db)
        {
            
        }
    }
}
