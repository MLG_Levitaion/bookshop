﻿using BookShopV2.DataAccessLayer.Models.Base;
using BookShopV2.DataAccessLayer.Models.Orders;
using DataAccessLayer.Entities;
using System.Threading.Tasks;

namespace BookShopV2.DataAccessLayer.Repositories.Interfaces
{
    public interface IOrderRepository :IBaseRepository<Order>
    {
        public Task<ResponseModel<ResponseOrderModelItem>> GetFilteredAsync(FilterOrderModel filter);
    }
}
