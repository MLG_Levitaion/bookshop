﻿using BookShopV2.DataAccessLayer.Models.Authors;
using BookShopV2.DataAccessLayer.Models.Base;
using DataAccessLayer.Entities;
using System.Threading.Tasks;

namespace BookShopV2.DataAccessLayer.Repositories.Interfaces
{
    public interface IAuthorRepository : IBaseRepository<Author>
    {
        public Task<ResponseModel<ResponseAuthorModelItem>> GetFilteredAsync(FilterAuthorModel filter);
    }
}
