﻿using BookShopV2.DataAccessLayer.Entities;
using BookShopV2.DataAccessLayer.Models.Base;
using BookShopV2.DataAccessLayer.Models.Users;
using System.Threading.Tasks;

namespace BookShopV2.DataAccessLayer.Repositories.Interfaces
{
    public interface IUserRepository
    {
        public Task<string> GenerateEmailConfirmTokenAsync(ApplicationUser user);
        public Task<bool> MarkAsRemovedAsync(long id);
        public Task<bool> UpdateAsync(ApplicationUser user);
        public Task<ApplicationUser> CreateAsync(ApplicationUser item, string password);
        public Task<bool> ConfirmEmail(ApplicationUser user, string token);
        public Task<bool> ChangePasswordAsync(ApplicationUser user, string oldPassword, string newPassword);
        public Task<bool> ResetPasswordAsync(ApplicationUser user, string newPassword);
        public Task<ApplicationUser> GetByEmailAsync(string email);
        public Task<bool> PasswordSignInAsync(ApplicationUser user, string password);
        public Task LogOutAsync();
        public Task<ResponseModel<UserModelItem>> GetFilteredAsync(FilterUserModel filter);
        public Task<string> GetUserRole(ApplicationUser user);
        public Task<ApplicationUser> GetById(long userId);
    }
}
