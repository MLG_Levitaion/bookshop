﻿using DataAccessLayer.Entities;
using System.Threading.Tasks;

namespace BookShopV2.DataAccessLayer.Repositories.Interfaces
{
    public interface IAuthorInPrintingEditionRepository : IBaseRepository<AuthorInPrintingEdition>
    {
        public Task<AuthorInPrintingEdition> GetByIdAsync(long authorId, long printingEditionId);
        public Task<bool> RemoveAsync(long authorId, long printingEditionId);
    }
}
