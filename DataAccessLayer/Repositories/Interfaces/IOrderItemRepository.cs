﻿using BookShopV2.DataAccessLayer.Models.Orders;
using DataAccessLayer.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookShopV2.DataAccessLayer.Repositories.Interfaces
{
    public interface IOrderItemRepository : IBaseRepository<OrderItem>
    {
        public Task<IEnumerable<OrderItem>> GetAllAsync(FilterOrderModel filter);
    }
}
