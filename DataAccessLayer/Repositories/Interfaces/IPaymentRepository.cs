﻿using DataAccessLayer.Entities;

namespace BookShopV2.DataAccessLayer.Repositories.Interfaces
{
    public interface IPaymentRepository : IBaseRepository<Payment>
    {       
    }
}
