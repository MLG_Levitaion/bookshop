﻿using BookShopV2.DataAccessLayer.Models.Base;
using BookShopV2.DataAccessLayer.Models.PrintingEditions;
using DataAccessLayer.Entities; 
using System.Threading.Tasks;

namespace BookShopV2.DataAccessLayer.Repositories.Interfaces
{
    public interface IPrintingEditionRepository : IBaseRepository<PrintingEdition>
    {
        public Task<ResponseModel<ResponsePrintingEditionModelItem>> GetFilteredAsync(FilterPrintingEditionModel filter);
    }
}
