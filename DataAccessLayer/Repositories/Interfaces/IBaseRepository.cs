﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookShopV2.DataAccessLayer.Repositories.Interfaces
{
    public interface IBaseRepository<T> where T : class
    {
        public Task<T> CreateAsync(T item);
        public IEnumerable<T> GetAll();
        public Task<T> GetByIdAsync(long id);
        public Task<bool> SaveChangesAsync(T item);
        public Task<bool> RemoveAsync(long id);
        public Task<bool> MarkAsRemovedAsync(long id);
        
    }
}
