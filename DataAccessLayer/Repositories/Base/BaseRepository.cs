﻿using BookShopV2.DataAccessLayer.Repositories.Interfaces;
using DataAccessLayer.AppContext;
using DataAccessLayer.Entities.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookShopV2.DataAccessLayer.Repositories.Base
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        protected readonly ApplicationDbContext _dbContext;
        protected readonly DbSet<T> _dbContextSet;

        public BaseRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
            _dbContextSet = dbContext.Set<T>();
        }

        public async Task<T> CreateAsync(T item)
        {
            item.CreationDate = DateTime.Now;
            await _dbContextSet.AddAsync(item);
            var result = await _dbContext.SaveChangesAsync();
            if (result < 0)
            {
                return null;
            }
            return item;
        }

        public IEnumerable<T> GetAll() 
        {
            return _dbContextSet.Where(t => t.IsRemoved == false);
        }

        public async Task<T> GetByIdAsync(long id)
        {
            var entity = await _dbContextSet.FindAsync(id);
            foreach (var navigation in _dbContext.Entry(entity).Navigations)
            {
                navigation.Load();
            }
            return entity;
        }

        public async Task<bool> MarkAsRemovedAsync(long id)
        {
            var item = await _dbContextSet.FindAsync(id);
            item.IsRemoved = true;
            return await _dbContext.SaveChangesAsync() > 0;
        }

        public async Task<bool> RemoveAsync(long id)
        {
            var item = await _dbContextSet.FindAsync(id);
            if (item == null)
            {
                return false;
            }
            _dbContextSet.Remove(item);
            return await _dbContext.SaveChangesAsync() > 0;
        }

        public async Task<bool> SaveChangesAsync(T item)
        {
            _dbContext.Entry(item).State = EntityState.Modified;
            return await _dbContext.SaveChangesAsync() > 0;

        }

    }
}
