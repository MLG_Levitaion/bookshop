﻿using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace BookShopV2.DataAccessLayer.Entities
{
    [NotMapped]
    public class ApplicationUser : IdentityUser<long>
    {
        public ApplicationUser()
        {
            Orders = new List<Order>();
        }
        public string ProfilePicture { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsRemoved { get; set; }
        public bool IsBlocked { get; set; }
        public DateTime CreationDate { get; set; }
        [JsonIgnore]
        public List<Order> Orders{ get; set; }
        
    }
}
