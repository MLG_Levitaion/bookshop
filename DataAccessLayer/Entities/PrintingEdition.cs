﻿using BookShopV2.DataAccessLayer.Entities.Enums;
using DataAccessLayer.Entities.Base;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace DataAccessLayer.Entities
{
    public class PrintingEdition : BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public OrderItemCurrency Currency { get; set; }
        public ProductType Type { get; set; }

        [JsonIgnore]
        public List<AuthorInPrintingEdition> AuthorInPrintingEditions { get; set; }

        [JsonIgnore]
        public List<OrderItem> OrderItems { get; set; }

        public PrintingEdition()
        {
            AuthorInPrintingEditions = new List<AuthorInPrintingEdition>();
            OrderItems = new List<OrderItem>();
        }



    }
}
