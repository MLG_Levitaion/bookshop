﻿namespace BookShopV2.DataAccessLayer.Entities.Enums
{
    public enum ProductType
    {
        Book = 1,
        Magazine = 2,
        NewsPaper = 3
    }
}
