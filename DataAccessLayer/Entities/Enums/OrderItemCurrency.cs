﻿namespace BookShopV2.DataAccessLayer.Entities.Enums
{
    public enum OrderItemCurrency
    {
        USD = 0,
        UAH = 1,
        EUR = 2,
        RUB = 3
    }
}
