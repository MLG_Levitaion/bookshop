﻿namespace BookShopV2.DataAccessLayer.Entities.Enums
{
    public enum OrderStatus
    {
        None =0,
        Unpaid = 1,
        Paid =2
    }
}
