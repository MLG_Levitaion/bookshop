﻿using DataAccessLayer.Entities.Base;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace DataAccessLayer.Entities
{
    public class Author : BaseEntity
    {
        public string Name { get; set; }

        [JsonIgnore]
        public List<AuthorInPrintingEdition> AuthorInPrintingEditions { get; set; }

        public Author()
        {
            AuthorInPrintingEditions = new List<AuthorInPrintingEdition>();
        }
    }

}

