﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookShopV2.DataAccessLayer.Entities
{
    [NotMapped]
    public class ApplicationRole : IdentityRole<long>
    {
    }
}
