﻿using BookShopV2.DataAccessLayer.Entities;
using BookShopV2.DataAccessLayer.Entities.Enums;
using DataAccessLayer.Entities.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace DataAccessLayer.Entities
{
    public class Order : BaseEntity
    {
        public string Description { get; set; }
        public OrderStatus Status { get; set; }
        public long UserId { get; set; }
        public ApplicationUser User { get; set; }
        public long PaymentId { get; set; }
        public Payment Payment { get; set; }

        [JsonIgnore]
        public List<OrderItem> OrderItems { get; set; }
        public Order()
        {
            OrderItems = new List<OrderItem>();
        }

    }
}
