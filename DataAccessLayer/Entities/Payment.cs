﻿using DataAccessLayer.Entities.Base;
using System.Text.Json.Serialization;

namespace DataAccessLayer.Entities
{
    public class Payment : BaseEntity
    {
        public string TransactionID { get; set; }
        public long OrderId { get; set; }

    }
}
