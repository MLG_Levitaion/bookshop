﻿namespace BookShopV2.DataAccessLayer.Initialize.Interfaces
{
    public interface IDbInitializer
    {
        public void Initialize();
    }
}
