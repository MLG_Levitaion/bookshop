﻿using BookShopV2.DataAccessLayer.Entities;
using BookShopV2.DataAccessLayer.Initialize.Interfaces;
using BookShopV2.DataAccessLayer.Repositories.EFRepositories;
using BookShopV2.DataAccessLayer.Repositories.Interfaces;
using DataAccessLayer.AppContext;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace BookShopV2.DataAccessLayer.Initialize
{
    public static class DbServiceInitializer
    {
        public static void Initialize(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
            });

            services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
            {
                options.Password.RequireNonAlphanumeric = Convert.ToBoolean(configuration["PasswordConfiguration:RequireNonAlphanumeric"]);
                options.Password.RequireDigit = Convert.ToBoolean(configuration["PasswordConfiguration:RequireDigit"]);
                options.Password.RequireLowercase = Convert.ToBoolean(configuration["PasswordConfiguration:RequireLowercase"]);
                options.Password.RequireUppercase = Convert.ToBoolean(configuration["PasswordConfiguration:RequireUppercase"]);
                options.Password.RequiredLength = Convert.ToInt32(configuration["PasswordConfiguration:RequiredLength"]);
                options.Password.RequiredUniqueChars = Convert.ToInt32(configuration["PasswordConfiguration:RequiredUniqueChars"]);
                options.SignIn.RequireConfirmedEmail = Convert.ToBoolean(configuration["PasswordConfiguration:RequireConfirmedEmail"]);
            }
           )
               .AddRoleManager<RoleManager<ApplicationRole>>()
               .AddEntityFrameworkStores<ApplicationDbContext>()
               .AddDefaultTokenProviders();

            services.AddScoped<IAuthorRepository, AuthorRepository>();
            services.AddScoped<IAuthorInPrintingEditionRepository, AuthorInPrintingEditionRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IOrderItemRepository, OrderItemRepository>();
            services.AddScoped<IPaymentRepository, PaymentRepository>();
            services.AddScoped<IPrintingEditionRepository, PrintingEditionRepository>();
            services.AddScoped<IUserRepository, UserRepository>();

            services.AddScoped<RoleManager<ApplicationUser>>();
            services.AddScoped<UserManager<ApplicationUser>>();
            services.AddScoped<IDbInitializer, DbInitializer>();

        }
    }
}
