﻿using BookShopV2.DataAccessLayer.Entities;
using BookShopV2.DataAccessLayer.Initialize.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace BookShopV2.DataAccessLayer.Initialize
{
    public class DbInitializer : IDbInitializer
    {

        private const string _adminPassword = "Admin322";
        private const string _adminFirstName = "Mykhailo";
        private const string _adminLastName = "Gavrilenko";
        private const string _adminUserName = "BookShopAdministrator";
        private const string _adminEmail = "admin@bookshop";
        private const string _userRole = "user";
        private const string _adminRole = "admin";

        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;

        public DbInitializer(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }

        public void Initialize()
        {
            var adminRole = _roleManager.FindByNameAsync(_adminRole).GetAwaiter().GetResult();

            if (adminRole == null)
            {
                adminRole = new ApplicationRole();
                adminRole.Name = _adminRole;
                _roleManager.CreateAsync(adminRole).GetAwaiter().GetResult();
            }

            var userRole = _roleManager.FindByNameAsync(_userRole).GetAwaiter().GetResult();

            if (userRole == null)
            {
                userRole = new ApplicationRole();
                userRole.Name = _userRole;
                _roleManager.CreateAsync(userRole).GetAwaiter().GetResult();
            }

            var result = _userManager.GetUsersInRoleAsync(_adminRole).GetAwaiter().GetResult();

            if (result.Count == 0)
            {
                var admin = new ApplicationUser();
                admin.Email = _adminEmail;
                admin.FirstName = _adminFirstName;
                admin.LastName = _adminLastName;
                admin.UserName = _adminUserName;
                admin.EmailConfirmed = true;

                _userManager.CreateAsync(admin, _adminPassword).GetAwaiter().GetResult();
                _userManager.AddToRoleAsync(admin, _adminRole).GetAwaiter().GetResult();
            }
        }
    }
}

