﻿using BookShopV2.DataAccessLayer.Entities.Enums;
using System.Collections.Generic;
using System.Linq;

namespace BookShopV2.DataAccessLayer.Extensions.PredicateBuilder
{
    public static partial class PredicateBuilder
    {
        public static string BuildPredicate(this IEnumerable<OrderStatus> enums, string field)
        {
            string predicate = string.Empty;

            foreach (var element in enums)
            {
                predicate += field + $" = {(int)element}";
                if (element != enums.LastOrDefault())
                {
                    predicate += Constants.OrStatement;
                }
            }

            return predicate;
        }
    }
}
