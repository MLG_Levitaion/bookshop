﻿namespace BookShopV2.DataAccessLayer.Extensions
{
    public static class Constants
    {
        public const string UserRole = "user";
        public const string DefaultField = "username";
        public const string AdminEmail = "admin@bookshop";
        public const string Type = "Type";
        public const string Status = "Status";
        public const string OrStatement = " || ";

    }
}
