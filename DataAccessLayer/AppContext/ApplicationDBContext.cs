﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using DataAccessLayer.Entities;
using BookShopV2.DataAccessLayer.Entities;

namespace DataAccessLayer.AppContext
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, long>
    {

        public DbSet<Payment> Payments { get; set; }
        public DbSet<PrintingEdition> PrintingEditions { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<AuthorInPrintingEdition> AuthorInPrintingEditions { get; set; }
        public DbSet<Author> Authors { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<AuthorInPrintingEdition>()
               .HasKey(c => new { c.AuthorId, c.PrintingEditionId });

            builder.Entity<AuthorInPrintingEdition>()
                .HasOne(sc => sc.Author)
                .WithMany(s => s.AuthorInPrintingEditions)
                .HasForeignKey(sc => sc.AuthorId);

            builder.Entity<AuthorInPrintingEdition>()
                .HasOne(sc => sc.PrintingEdition)
                .WithMany(c => c.AuthorInPrintingEditions)
                .HasForeignKey(sc => sc.PrintingEditionId);

            builder.Entity<OrderItem>()
               .HasKey(c => new { c.OrderId, c.PrintingEditionId });

            builder.Entity<OrderItem>()
                .HasOne(sc => sc.Order)
                .WithMany(c => c.OrderItems)
                .HasForeignKey(sc => sc.OrderId);

            builder.Entity<OrderItem>()
                .HasOne(sc => sc.PrintingEdition)
                .WithMany(c => c.OrderItems)
                .HasForeignKey(sc => sc.PrintingEditionId);

            builder.Entity<Order>()
                .HasOne(u => u.User)
                .WithMany(o => o.Orders)
                .HasForeignKey(f => f.UserId);              

            base.OnModelCreating(builder);
        }
    }
}
