﻿using BookShopV2.DataAccessLayer.Models.Enums;

namespace BookShopV2.DataAccessLayer.Models.Base
{
    public class FilterBaseModel
    {
        public string SearchString { get; set; } = string.Empty;
        public int Page { get; set; } = 1;
        public int PageSize { get; set; } = 10;
        public string SortField { get; set; } = "id";
        public SortBy SortBy { get; set; }
    }
}
