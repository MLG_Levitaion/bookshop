﻿using BookShopV2.DataAccessLayer.Entities.Enums;

namespace BookShopV2.DataAccessLayer.Models.OrderItems
{
    public class ResponseOrderItemModelItem
    {
        public string Title { get; set; }
        public ProductType ProductType { get; set; }
        public int Count { get; set; }
        public decimal Amount { get; set; }
    }
}
