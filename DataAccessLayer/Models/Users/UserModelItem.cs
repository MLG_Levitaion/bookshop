﻿namespace BookShopV2.DataAccessLayer.Models.Users
{
    public class UserModelItem
    {
        public long Id { get; set; }
        public string ProfilePicture { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Role { get; set; }
        public bool IsBlocked { get; set; }
    }
}
