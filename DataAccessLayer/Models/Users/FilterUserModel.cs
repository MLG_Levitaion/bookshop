﻿using BookShopV2.DataAccessLayer.Models.Base;

namespace BookShopV2.DataAccessLayer.Models.Users
{
    public class FilterUserModel : FilterBaseModel
    {
        public bool IsBlocked { get; set; }
    }
}
