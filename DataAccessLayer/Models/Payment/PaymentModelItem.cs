﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookShopV2.DataAccessLayer.Models.Payment
{
    public class PaymentModelItem
    {
        public long OrderId { get; set; }
        public string PaymentId { get; set; }
    }
}
