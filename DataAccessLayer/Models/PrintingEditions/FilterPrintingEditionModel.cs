﻿using BookShopV2.DataAccessLayer.Entities.Enums;
using BookShopV2.DataAccessLayer.Models.Base;
using System.Collections.Generic;

namespace BookShopV2.DataAccessLayer.Models.PrintingEditions
{
    public class FilterPrintingEditionModel : FilterBaseModel
    {
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
        public IEnumerable<ProductType> PrintingEditionTypes { get; set; } = new List<ProductType>();
        public OrderItemCurrency Currency { get; set; }
       
    }
}
