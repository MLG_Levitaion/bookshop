﻿using BookShopV2.DataAccessLayer.Entities.Enums;
using BookShopV2.DataAccessLayer.Models.Authors;
using System.Collections.Generic;

namespace BookShopV2.DataAccessLayer.Models.PrintingEditions
{
    public class ResponsePrintingEditionModelItem
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public OrderItemCurrency Currency { get; set; }
        public ProductType Type { get; set; }
        public IEnumerable<AuthorModelItem> Authors { get; set; } = new List<AuthorModelItem>();
    }
}
