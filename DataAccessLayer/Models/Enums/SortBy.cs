﻿namespace BookShopV2.DataAccessLayer.Models.Enums
{
    public enum SortBy
    {
        None = 0,
        Ascending = 1,
        Descending = 2
    }
}