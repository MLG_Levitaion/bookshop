﻿using BookShopV2.DataAccessLayer.Entities.Enums;
using BookShopV2.DataAccessLayer.Models.Base;
using System.Collections.Generic;

namespace BookShopV2.DataAccessLayer.Models.Orders
{
    public class FilterOrderModel : FilterBaseModel
    {
        public OrderItemCurrency Currency { get; set; }
        public IEnumerable<OrderStatus> Status { get; set; }
        public int IdUser { get; set; }
    }
}
