﻿using BookShopV2.DataAccessLayer.Entities.Enums;
using BookShopV2.DataAccessLayer.Models.OrderItems;
using System;
using System.Collections.Generic;

namespace BookShopV2.DataAccessLayer.Models.Orders
{
    public class ResponseOrderModelItem
    {
        public long Id { get; set; }
        public DateTime CreationDate { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public decimal OrderAmount { get; set; }
        public OrderStatus Status { get; set; }
        public IEnumerable<ResponseOrderItemModelItem> OrderItems { get; set; } = new List<ResponseOrderItemModelItem>();
    }
}
