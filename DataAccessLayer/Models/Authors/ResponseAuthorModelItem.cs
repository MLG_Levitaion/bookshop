﻿using System.Collections.Generic;

namespace BookShopV2.DataAccessLayer.Models.Authors
{
    public class ResponseAuthorModelItem: AuthorModelItem
    {
        public IEnumerable<string> PrintingEditions { get; set; } = new List<string>();
    }
}
