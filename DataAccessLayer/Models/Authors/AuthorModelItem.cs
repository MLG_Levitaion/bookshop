﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookShopV2.DataAccessLayer.Models.Authors
{
   public class AuthorModelItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
