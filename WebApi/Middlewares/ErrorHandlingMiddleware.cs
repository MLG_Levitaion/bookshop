﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace BookShopV2.PresentationLayer.Middlewares
{
    public class ErrorHandlingMiddleware //todo inject Logger
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ErrorHandlingMiddleware> _logger;
        public ErrorHandlingMiddleware(RequestDelegate next, ILogger<ErrorHandlingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }
        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next.Invoke(context); //todo use try-catch
                if (context.Response.StatusCode == 403)
                {
                    await context.Response.WriteAsync("Not Authorized");
                }
                if (context.Response.StatusCode == 401)
                {
                    await context.Response.WriteAsync("Access denied");
                }
                if (context.Response.StatusCode == 404)
                {
                    await context.Response.WriteAsync("Not Found");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($@"Following error occured : {ex.Message}");
            }
        }
    }
}
