﻿using BookShopV2.BusinessLogicLayer.Models.Users;
using System.Security.Claims;

namespace BookShopV2.PresentationLayer.Helpers.Interfaces
{
    public interface IJwtHelper
    {
        public string GenerateAccessToken(UserModelItem user);
        public string GenerateRefreshToken(UserModelItem user);
        public UserModelItem GetUserFromToken(string token);
        public ClaimsPrincipal ValidateRefreshtoken(string refreshtoken);
    }
}
