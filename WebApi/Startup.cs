using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using BookShopV2.PresentationLayer.Middlewares;
using BookShopV2.PresentationLayer.Helpers.Interfaces;
using BookShopV2.PresentationLayer.Helpers;
using BookShopV2.BusinessLogicLayer.Initializers;
using BookShopV2.DataAccessLayer.Initialize.Interfaces;
using NLog.Extensions.Logging;

namespace BookShopV2.PresentationLayer
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Initialize(Configuration);
            services.AddScoped<IJwtHelper, JwtHelper>();
        }

        [Obsolete]
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IDbInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHsts();

            loggerFactory.ConfigureNLog("nlog.config");
            loggerFactory.AddNLog();

            app.UseMiddleware<ErrorHandlingMiddleware>();

            app.UseCors(options => options.SetIsOriginAllowed((host) => true).AllowAnyMethod().AllowAnyHeader().AllowCredentials())
                        .UseSwagger()
                        .UseSwaggerUI(c =>
                        {
                            c.SwaggerEndpoint(Configuration["SwaggerConfiguration:Path"], Configuration["SwaggerConfiguration:Name"]);
                        });
                       
            app.UseRouting();
            app.UseDefaultFiles();
            app.UseFileServer();
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseAuthentication();
            app.UseAuthorization();
                       
            app.UseEndpoints(
                endpoint => endpoint.MapControllers()
            );
            dbInitializer.Initialize();
        }


    }
}