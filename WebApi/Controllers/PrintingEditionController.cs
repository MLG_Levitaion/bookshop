﻿using System.Threading.Tasks;
using BookShopV2.BusinessLogicLayer.Extensions.ConstantsExtension.RolesConstantsExtension;
using BookShopV2.BusinessLogicLayer.Models.PrintingEditions;
using BookShopV2.BusinessLogicLayer.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookShopV2.PresentationLayer.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class PrintingEditionController : ControllerBase
    {
        private readonly IPrintingEditionService _printingEditionService;

        public PrintingEditionController(IPrintingEditionService printingEditionService)
        {
            _printingEditionService = printingEditionService;
        }

        [HttpPost("create")]
        [Authorize(Roles = RolesConstants.AdminRole)]
        public async Task<IActionResult> CreatePrintingEdition(PrintingEditionModelItem printingEditionModel)
        {
            return Ok(await _printingEditionService.CreateAsync(printingEditionModel));
        }

        [HttpDelete("delete")]
        [Authorize(Roles = RolesConstants.AdminRole)]
        public async Task<IActionResult> Delete(long id)
        {
            return Ok(await _printingEditionService.DeleteAsync(id));
        }
        
        [HttpPut("update")]
        [Authorize(Roles = RolesConstants.AdminRole)]
        public async Task<IActionResult> Update(PrintingEditionModelItem printingEditionModel)
        {
            return Ok(await _printingEditionService.UpdateAsync(printingEditionModel));
        }

        [HttpPost("convert")]
        [AllowAnonymous]
        public async Task<IActionResult> Convert(ConvertPriceModel convertPriceModel)
        {
            return Ok(_printingEditionService.Convert(convertPriceModel));
        }

        [HttpPost("getall")]
        [AllowAnonymous]
        public async Task<IActionResult> GetAll(FilterPrintingEditionModel filter)
        {
            return Ok(await _printingEditionService.GetAllAsync(filter));
        }
    }
}