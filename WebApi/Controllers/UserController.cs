﻿using BookShopV2.BusinessLogicLayer.Extensions.ConstantsExtension.RolesConstantsExtension;
using BookShopV2.BusinessLogicLayer.Models.Users;
using BookShopV2.BusinessLogicLayer.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookShopV2.PresentationLayer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("getall")]
        [Authorize(Roles = RolesConstants.AdminRole)]
        public async Task<IActionResult> GetAll(FilterUserModel filter)
        {
            return Ok(await _userService.GetAllAsync(filter));
        }

        [HttpGet("getuser")]
        [Authorize]
        public async Task<IActionResult> GetUser(long id)
        {
            return Ok(await _userService.GetAsync(id));
        }

        [HttpPut("update")]
        [Authorize]
        public async Task<IActionResult> Update(UserModelItem userModel)
        {
            return Ok(await _userService.UpdateAsync(userModel));
        }

        [HttpDelete("delete")]
        [Authorize(Roles = RolesConstants.AdminRole)]
        public async Task<IActionResult> Delete(long id)
        {
            return Ok(await _userService.DeleteAsync(id));
        }
    }
}