﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using BookShopV2.BusinessLogicLayer.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using BookShopV2.PresentationLayer.Helpers.Interfaces;
using System.Linq;
using BookShopV2.BusinessLogicLayer.Models.Account;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System;
using System.Security.Claims;
using BookShopV2.BusinessLogicLayer.Extensions.ConstantsExtension.RolesConstantsExtension;

namespace BookShopV2.PresentationLayer.Controllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private const string _userRole = "user";

        private readonly IAccountService _accountService;
        private readonly IJwtHelper _jwtHelper;
        private readonly IConfiguration _configuration;

        public AccountController(IAccountService accountService, IJwtHelper jwtHelper, IConfiguration configuration)
        {
            _accountService = accountService;
            _jwtHelper = jwtHelper;
            _configuration = configuration;
        }

        [HttpGet("test")]
        [AllowAnonymous]
        public IActionResult Test()
        {
            var test = "yolo";
            return Ok(test);
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginModel loginModel)
        {
            var returnModel = await _accountService.LoginAsync(loginModel);
            if (returnModel.Errors.Any())
            {
                return Ok(returnModel);
            }

            string accessToken = _jwtHelper.GenerateAccessToken(returnModel);
            string refreshToken = _jwtHelper.GenerateRefreshToken(returnModel);
            HttpContext.Response.Cookies.Append("AccessToken", accessToken);
            HttpContext.Response.Cookies.Append("RefreshToken", refreshToken);
            return Ok(returnModel);

        }

        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register(RegisterModel registerModel)
        {
            return Ok(await _accountService.RegisterAsync(registerModel));
        }

        [HttpGet("emailconfirm")]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(long id, string token)
        {

            var userModel = await _accountService.ConfirmEmailAsync(id, token);
            if (!userModel.Errors.Any())
            {
                return Redirect(_configuration["Client"] + "account/emailconfirm");
            }

            return Ok(userModel);
        }
        [HttpPost("forgotpassword")]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordModel forgotPasswordModel)
        {
            return Ok(await _accountService.EmailForgotPasswordAsync(forgotPasswordModel.Email));
        }

        [HttpPost("changepassword")]
        [Authorize]
        public async Task<IActionResult> ChangePassword(ChangePasswordModel changePasswordModel)
        {
            return Ok(await _accountService.ChangePasswordAsync(changePasswordModel));
        }

        [HttpPost("refreshtokens")]
        [AllowAnonymous]
        public async Task<RefreshTokenModel> RefreshTokens(RefreshTokenModel tokenModel)
        {
            var user = _jwtHelper.GetUserFromToken(tokenModel.AccessToken);
            var refreshPrincipal = _jwtHelper.ValidateRefreshtoken(tokenModel.RefreshToken);
            if (refreshPrincipal == null)
            {
                await _accountService.LogOutAsync();
                Redirect(_configuration["Client"] + "account/login");
                return null;
            }
            var refreshTokenUserId = Convert.ToInt64(refreshPrincipal.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value);

            if (user.Id != refreshTokenUserId)
            {
                await _accountService.LogOutAsync();
                Redirect(_configuration["Client"] + "account/login");
                return null;
            }

            tokenModel.AccessToken = _jwtHelper.GenerateAccessToken(user);
            tokenModel.RefreshToken = _jwtHelper.GenerateRefreshToken(user);
            return tokenModel;
        }

        [HttpGet("logout")]
        [Authorize]
        public async Task LogOut()
        {
            await _accountService.LogOutAsync();
            Redirect(_configuration["Client"] + "account/login");
        }
    }
}
