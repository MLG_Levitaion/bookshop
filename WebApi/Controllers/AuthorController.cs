﻿using System.Threading.Tasks;
using BookShopV2.BusinessLogicLayer.Extensions.ConstantsExtension.RolesConstantsExtension;
using BookShopV2.BusinessLogicLayer.Models.Authors;
using BookShopV2.BusinessLogicLayer.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookShopV2.PresentationLayer.Controllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    public class AuthorController : ControllerBase
    {
        private readonly IAuthorService _authorService;

        public AuthorController(IAuthorService authorService)
        {
            _authorService = authorService;
        }

        [HttpPost("getauthors")]
        [Authorize(Roles = RolesConstants.AdminRole)]
        public async Task<IActionResult> GetAllAuthors(FilterAuthorModel filterModel)
        {           
            return Ok(await _authorService.GetFilteredAsync(filterModel));
        }

        [HttpGet("getall")]
        [Authorize(Roles = RolesConstants.AdminRole)]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _authorService.GetAllAsync());
        }

        [HttpPost("create")]        
        [Authorize(Roles = RolesConstants.AdminRole)]
        public async Task<IActionResult> AddAuthor(AuthorModelItem author)
        {
            var result = await _authorService.CreateAsync(author);
            return Ok(result);
        }

        [HttpPut("update")]
        [Authorize(Roles = RolesConstants.AdminRole)]
        public async Task<IActionResult> Update(AuthorModelItem authorModel)
        {            
             return Ok(await _authorService.UpdateAsync(authorModel));            
        }

        [HttpDelete("delete")]
        [Authorize(Roles = RolesConstants.AdminRole)]
        public async Task<IActionResult> DeleteAuthor(long id)
        {
            return Ok(await _authorService.DeleteAsync(id));
        }

    }
}