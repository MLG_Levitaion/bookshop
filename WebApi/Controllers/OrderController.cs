﻿using System.Threading.Tasks;
using BookShopV2.BusinessLogicLayer.Extensions.ConstantsExtension.RolesConstantsExtension;
using BookShopV2.BusinessLogicLayer.Models.Orders;
using BookShopV2.BusinessLogicLayer.Models.Payments;
using BookShopV2.BusinessLogicLayer.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookShopV2.PresentationLayer.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class OrderController : ControllerBase //todo check this
    {

        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpPost("getfiltered")]
        [Authorize]
        public async Task<IActionResult> GetFiltered(FilterOrderModel filter)
        {
            var res = await _orderService.GetFilteredAsync(filter);
            return Ok(res);
        }
        
        [HttpPost("create")]
        [Authorize(Roles = RolesConstants.UserRole)]
        public async Task<IActionResult> Create(RequestOrderModelItem orderModel)
        {
            return Ok(await _orderService.CreateAsync(orderModel));
        }

        [HttpGet("get")]
        [Authorize(Roles =RolesConstants.UserRole)]
        public async Task<IActionResult> Get(long id)
        {
            return Ok(await _orderService.GetAsync(id));
        }

        [HttpPost("pay")]
        [Authorize(Roles = RolesConstants.UserRole)]
        public async Task<IActionResult> Pay(PaymentModelItem paymentModel)
        {
            return Ok(await _orderService.UpdateAsync(paymentModel));
        }
    }
}